//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ActividadClase_mvc.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Modelo
    {
        public int modelo_id { get; set; }
        public string mod_status { get; set; }
        public string mod_descripcion { get; set; }
        public System.DateTime mod_add { get; set; }
        public Nullable<System.DateTime> mod_update { get; set; }
        public Nullable<System.DateTime> mod_delete { get; set; }
        public Nullable<int> mar_id { get; set; }
    }
}
