//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ActividadClase_mvc.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Matricula
    {
        public long mat_id { get; set; }
        public System.DateTime mat_fechaemision { get; set; }
        public Nullable<System.DateTime> mat_fechacaduca { get; set; }
        public string mat_numeroespecie { get; set; }
        public decimal mat_valor { get; set; }
        public Nullable<int> can_id { get; set; }
        public string per_identificacion { get; set; }
    
        public virtual Canton Canton { get; set; }
    }
}
