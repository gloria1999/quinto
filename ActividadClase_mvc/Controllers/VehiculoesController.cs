﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ActividadClase_mvc.Models;

namespace ActividadClase_mvc.Controllers
{
    public class VehiculoesController : Controller
    {
        private BDMATRICULAEntities db = new BDMATRICULAEntities();

        // GET: Vehiculoes
        public async Task<ActionResult> Index()
        {
    
            return View(await db.Vehiculo.Where(data=> 
                        data.veh_status.Equals("A")).ToListAsync());
        }

        // GET: Vehiculoes/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vehiculo vehiculo = await db.Vehiculo.FindAsync(id);
            if (vehiculo == null)
            {
                return HttpNotFound();
            }
            return View(vehiculo);
        }

        // GET: Vehiculoes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Vehiculoes/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "veh_id,veh_placaanterior,veh_placaactual,veh_chasis,veh_motor,veh_tonelaje,veh_fechacompra,veh_pasajeros,veh_carroceria,veh_combustible,veh_observacion,veh_fechaemision,veh_fechacaducidad,vehiculo_anio,veh_cilindraje,veh_status,veh_add,veh_update,veh_delete,cla_id,modelo_id,tip_id,col_id,pai_id")] Vehiculo vehiculo)
        {
            if (ModelState.IsValid)
            {
                db.Vehiculo.Add(vehiculo);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(vehiculo);
        }

        // GET: Vehiculoes/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vehiculo vehiculo = await db.Vehiculo.FindAsync(id);
            if (vehiculo == null)
            {
                return HttpNotFound();
            }
            return View(vehiculo);
        }

        // POST: Vehiculoes/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "veh_id,veh_placaanterior,veh_placaactual,veh_chasis,veh_motor,veh_tonelaje,veh_fechacompra,veh_pasajeros,veh_carroceria,veh_combustible,veh_observacion,veh_fechaemision,veh_fechacaducidad,vehiculo_anio,veh_cilindraje,veh_status,veh_add,veh_update,veh_delete,cla_id,modelo_id,tip_id,col_id,pai_id")] Vehiculo vehiculo)
        {
            if (ModelState.IsValid)
            {
                db.Entry(vehiculo).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(vehiculo);
        }

        // GET: Vehiculoes/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vehiculo vehiculo = await db.Vehiculo.FindAsync(id);
            if (vehiculo == null)
            {
                return HttpNotFound();
            }
            return View(vehiculo);
        }

        // POST: Vehiculoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Vehiculo vehiculo = await db.Vehiculo.FindAsync(id);
            db.Vehiculo.Remove(vehiculo);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
