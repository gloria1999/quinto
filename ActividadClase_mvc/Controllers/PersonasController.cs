﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ActividadClase_mvc.Models;

namespace ActividadClase_mvc.Controllers
{
    public class PersonasController : Controller
    {
        private BDMATRICULAEntities db = new BDMATRICULAEntities();

        // GET: Personas
        public async Task<ActionResult> Index()
        {
            return View(await db.Persona.Where(data =>
                                                data.per_status.Equals("A")).ToListAsync());
        }
        // GET: Personas/Details/5
        public async Task<ActionResult> Details(long? id, string identificacion)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Persona persona = await db.Persona.FindAsync(id, identificacion);
            if (persona == null)
            {
                return HttpNotFound();
            }
            return View(persona);
        }
        //GET: Personas/Create
        public ActionResult Create()
        {
            List<SelectListItem> listaTipoIdentificacion = new List<SelectListItem>()
            {
                new SelectListItem{Text="Seleccione Tipo", Value="0"},
                new SelectListItem{Text="Cedula", Value="C"},
                new SelectListItem{Text="Ruc", Value="R"},
                new SelectListItem{Text="Pasaporte", Value="P"},
                new SelectListItem{Text="Otros", Value="O"},
            };
            ViewBag.ListItemTipo = listaTipoIdentificacion;

            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "per_id,per_identificacion,per_tipoidentificacion,per_apellidos,per_nombres,per_fechanacimiento,per_direccion,per_telefono,per_celular,per_tiposangre,per_correo,per_genero,per_status,per_add,per_update,per_delete")] Persona persona, string ListItemTipo)
        {
            if (ModelState.IsValid)
            {
                persona.per_id = getnextsequenceValue();
                persona.per_status = "A";
                persona.per_add = DateTime.Now;
                persona.per_tipoidentificacion = ListItemTipo;

                db.Persona.Add(persona);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            List<SelectListItem> listaTipoIdentificacion = new List<SelectListItem>()
            {
                new SelectListItem{Text="Seleccione Tipo", Value="0"},
                new SelectListItem{Text="Cedula", Value="C"},
                new SelectListItem{Text="Ruc", Value="R"},
                new SelectListItem{Text="Pasaporte", Value="P"},
                new SelectListItem{Text="Otros", Value="O"},
            };
            ViewBag.ListItemTipo = listaTipoIdentificacion;
            return View(persona);
        }
        
        private long getnextsequenceValue()
        {
            try
            {
                var query = db.Database.SqlQuery<long>("SELECT NEXT VALUE FOR [dbo].[sq_Persona]");
                var taskQuery = query.SingleOrDefaultAsync();
                var sequence = taskQuery.Result;
                return sequence;
            }
            catch
            {
                return 0;
            }
        }
        // GET: Personas/Edit/5
        public async Task<ActionResult> Edit(long? id, string identificacion)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Persona persona = await db.Persona.FindAsync(id, identificacion);
            if (persona == null)
            {
                return HttpNotFound();
            }
            return View(persona);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "per_id,per_identificacion,per_tipoidentificacion,per_apellidos,per_nombres,per_fechanacimiento,per_direccion,per_telefono,per_celular,per_tiposangre,per_correo,per_genero,per_status,per_add,per_update,per_delete")] Persona persona)
        {
            if (ModelState.IsValid)
            {
                db.Entry(persona).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(persona);
        }

        // GET: Personas/Delete/5
        public async Task<ActionResult> Delete(long? id, string identificacion)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Persona persona = await db.Persona.FindAsync(id, identificacion);
            if (persona == null)
            {
                return HttpNotFound();
            }
            return View(persona);
        }

        // POST: Personas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(long id)
        {
            Persona persona = await db.Persona.FindAsync(id);
            db.Persona.Remove(persona);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
