﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ActividadClase_mvc.Startup))]
namespace ActividadClase_mvc
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
