﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucGridViewGeneral.ascx.cs" Inherits="MatriculacionVehicular.userControl.ucGridViewGeneral" %>
<asp:GridView ID="GridView1" Aligth="center" runat="server" CellPadding="2" ForeColor="Black" GridLines="None" OnRowCommand="GridView1_RowCommand" BackColor="LightGoldenrodYellow" BorderColor="Tan" BorderWidth="1px">
     <AlternatingRowStyle BackColor="PaleGoldenrod" />
     <Columns>
        <asp:TemplateField>
            <ItemTemplate>
                <asp:ImageButton ID="imgModificar" runat="server" ImageUrl="~/images/modificar.png" Width="32px" Height="32px" CommandName="Modificar" CommandArgument='<%#Eval("ID") %>'/>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField>
            <ItemTemplate>
                <asp:ImageButton ID="imgEliminar" runat="server" ImageUrl="~/images/icon_delete.png" Width="32px" Height="32px" CommandName="Eliminar" CommandArgument='<%#Eval("ID") %>'  />
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>

     <FooterStyle BackColor="Tan" />
    <HeaderStyle BackColor="Tan" Font-Bold="True" />
    <PagerStyle BackColor="PaleGoldenrod" ForeColor="DarkSlateBlue" HorizontalAlign="Center" />
    <SelectedRowStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite" />
    <SortedAscendingCellStyle BackColor="#FAFAE7" />
    <SortedAscendingHeaderStyle BackColor="#DAC09E" />
    <SortedDescendingCellStyle BackColor="#E1DB9C" />
    <SortedDescendingHeaderStyle BackColor="#C2A47B" />
</asp:GridView>
