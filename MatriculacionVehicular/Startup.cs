﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MatriculacionVehicular.Startup))]
namespace MatriculacionVehicular
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
