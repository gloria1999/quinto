﻿using MatriculacionVehicular.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace MatriculacionVehicular.Logica
{
    public class LogicaVehiculos
    {
        private static BDMATRICULAEntities db = new BDMATRICULAEntities();

        public static async Task<List<Vehiculo>> getVehiculo()
        {
            try
            {
                return await db.Vehiculo.Where(data => data.veh_status == "A").ToListAsync();
            }
            catch (Exception ex)

            {
                throw new ArgumentException("Error al obtener vehiculo");
            }

        }
        public static async Task<Vehiculo> getVehiculoxId(int idVehiculo)
        {
            try
            {
                return await db.Vehiculo.FirstOrDefaultAsync(data => data.veh_status == "A"
                        && data.veh_id.Equals(idVehiculo));
            }
            catch (Exception ex)

            {
                throw new ArgumentException("Error al obtener vehiculo");
            }

        }
        public static async Task<bool> saveVehiculo(Vehiculo dataVehiculo)
        {
            try
            {

                bool result = false;

                dataVehiculo.veh_status = "A";
                dataVehiculo.veh_add = DateTime.Now;
                db.Vehiculo.Add(dataVehiculo);
                // dataProducto.Add(dataProducto);
                result = await db.SaveChangesAsync() > 0;
                //await db.SaveChangesAsync();
                //result = true;
                return result;
            }
            catch (Exception ex)

            {
                throw new ArgumentException("Error al guardar vehiculo");
            }

        }


        public static async Task<bool> updateVehiculo(Vehiculo dataVehiculo)
        {
            try
            {

                bool result = false;

                //dataProducto.pro_status = "A";
                dataVehiculo.veh_update = DateTime.Now;
                // db.TBL_PRODUCTO.Add(dataProducto);
                result = await db.SaveChangesAsync() > 0;
                // result = true;
                return result;
            }
            catch (Exception ex)

            {
                throw new ArgumentException("Error al modificar producto");
            }

        }

        public static async Task<bool> deleteVehiculo(Vehiculo dataVehiculo)
        {
            try
            {
                bool result = false;

                dataVehiculo.veh_status = "I";
                dataVehiculo.veh_add = DateTime.Now;
                result = await db.SaveChangesAsync() > 0;
                return result;
            }
            catch (Exception ex)

            {
                throw new ArgumentException("Error al guardar vehiculo");
            }

        }


    }
}
