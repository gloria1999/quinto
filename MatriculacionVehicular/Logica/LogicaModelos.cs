﻿using MatriculacionVehicular.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace MatriculacionVehicular.Logica
{
    public class LogicaModelos
    {
        private static BDMATRICULAEntities db = new BDMATRICULAEntities();

        public static async Task<List<Modelo>> getModelo()
        {
            try
            {
                return await db.Modelo.Where(data => data.mod_status == "A").ToListAsync();
            }
            catch (Exception ex)

            {
                throw new ArgumentException("Error al obtener modelo");
            }
        }

        public static async Task<Modelo> getModeloxId(int idModelo)
        {
            try
            {
                return await db.Modelo.FirstOrDefaultAsync(data => data.mod_status == "A"
                        && data.modelo_id.Equals(idModelo));
            }
            catch (Exception ex)

            {
                throw new ArgumentException("Error al obtener modelo");
            }

        }
        public static async Task<bool> saveModelo(Modelo dataModelo)
        {
            try
            {

                bool result = false;

                dataModelo.mod_status = "A";
                dataModelo.mod_add = DateTime.Now;
                db.Modelo.Add(dataModelo);
                // dataProducto.Add(dataProducto);
                result = await db.SaveChangesAsync() > 0;
                // result = true;
                return result;
            }
            catch (Exception ex)

            {
                throw new ArgumentException("Error al guardar modelo");
            }

        }
        public static async Task<bool> updateModelo(Modelo dataModelo)
        {
            try
            {

                bool result = false;

                //dataProducto.pro_status = "A";
                dataModelo.mod_update = DateTime.Now;
                // db.TBL_PRODUCTO.Add(dataProducto);
                result = await db.SaveChangesAsync() > 0;
                // result = true;
                return result;
            }
            catch (Exception ex)

            {
                throw new ArgumentException("Error al modificar modelo");
            }

        }
        public static async Task<bool> deleteModelo(Modelo dataModelo)
        {
            try
            {

                bool result = false;

                dataModelo.mod_status = "I";
                dataModelo.mod_delete = DateTime.Now;
                //db.TBL_PRODUCTO.Add(dataProducto);

                //result = await db.SaveChangesAsync() > 0;
                //eliminar datos de forma fisisca 
                //db.TBL_PRODUCTO.Remove(dataProducto);
                //commit hacia la base d edatos
                result = await db.SaveChangesAsync() > 0;
                //result = true;
                return result;
            }
            catch (Exception ex)

            {
                throw new ArgumentException("Error al guardar modelo");
            }

        }
    }
}