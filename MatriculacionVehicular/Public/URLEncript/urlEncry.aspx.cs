﻿using MatriculacionVehicular.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MatriculacionVehicular.Public.URLEncript
{
    public partial class urlEncry : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                loadClase();
            }

        }
        private void loadClase()
        {
            try
            {
                var taskListClase = Task.Run(() => Logica.LogicaClase.getClases());
                taskListClase.Wait();

                var listClase = taskListClase.Result;

                if (listClase.Count > 0 && listClase != null)
                {
                    gdvDatosClase.DataSource = listClase.Select(data => new
                    {
                        ID = data.cla_id,
                        DESCRIPCION = data.cla_descripcion,
                        FECHA = data.cla_add,
                        ESTADO = data.cla_status
                    }).ToList();
                    gdvDatosClase.DataBind();

                }
            }
            catch (Exception ex)
            {
            }

        }
        private string Encrypt(string clearText)
        {
            string EncryptionKey = "MAKV2SPBNI99212";
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;
        }

        protected void gdvDatosClase_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                string image = HttpUtility.UrlEncode(Encrypt(e.CommandArgument.ToString()));
                string codigoClase = Convert.ToString(e.CommandArgument);
                if (e.CommandName.Equals("Modificar"))
                {

                    //ENCRIPTAR EN TIPO HASH O OTRO ESO SE TIENE COMO MOSTRAR JUNTO CON LA URL EL codigoClase de la URL en string encrptado minuto 12.44
                    Response.Redirect("urlEncrypNuevo.aspx?cod=" + image);
                }
                else if (e.CommandName.Equals("Eliminar"))
                {
                    Clase dataclase = new Clase();
                    var taskclase = Task.Run(() => Logica.LogicaClase.getClasexId(int.Parse(codigoClase)));
                    taskclase.Wait();
                    dataclase = taskclase.Result;
                    if (dataclase != null)
                    {
                        var taskdelete = Task.Run(() => Logica.LogicaClase.deleteClase(dataclase));
                        taskdelete.Wait();
                        if (taskdelete.Result)
                        {
                            loadClase();
                        }
                    }


                }


            }
            catch (Exception ex)
            {

                throw;
            }

        }

        protected void imgcNuevo_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("urlEncrypNuevo.aspx");
        }

        protected void lnkcNuevo_Click(object sender, EventArgs e)
        {
            Response.Redirect("urlEncrypNuevo.aspx");
        }
    }
}