﻿using MatriculacionVehicular.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MatriculacionVehicular.Public.URLEncript
{
    public partial class urlEncrypNuevo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                string id = Request["cod"];
                if (id != null)
                {
                    ucCargarDatos();
                }


            }

        }
        private string Decrypt(string cipherText)
        {
            string EncryptionKey = "MAKV2SPBNI99212";
            cipherText = cipherText.Replace(" ", "+");
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }
        private void ucCargarDatos()
        {

            string decry = Decrypt(HttpUtility.UrlDecode(Request.QueryString["cod"]));
            var taskUsuarioxId = Task.Run(() => Logica.LogicaClase.getClasexId(Convert.ToInt32(decry)));
            taskUsuarioxId.Wait();
            var usuario = taskUsuarioxId.Result;
            lblId.Text = usuario.cla_id.ToString();
            txtDescripcion.Text = usuario.cla_descripcion.ToString();

        }
        private void save()
        {
            try
            {
                Clase dataProducto = new Clase();
                dataProducto.cla_descripcion = txtDescripcion.Text;
                var taskSave = Task.Run(() => Logica.LogicaClase.saveClase(dataProducto));
                taskSave.Wait();
                if (taskSave.Result)
                {
                    lblMessage.Text = "clase guardado correctamente";
                    newClase();
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
        }

        private void update()
        {
            try
            {
                Clase dataProducto = new Clase();
                var tasProducto = Task.Run(() => Logica.LogicaClase.getClasexId(int.Parse(lblId.Text)));
                tasProducto.Wait();
                dataProducto = tasProducto.Result;
                if (dataProducto != null)
                {
                    dataProducto.cla_descripcion = txtDescripcion.Text;
                    var taskSave = Task.Run(() => Logica.LogicaClase.updateClase(dataProducto));
                    taskSave.Wait();
                    if (taskSave.Result)
                    {
                        lblMessage.Text = "Clase guardado correctamente";
                        newClase();
                    }
                }


            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
        }

        private void newClase()
        {
            lblId.Text = "";
            txtDescripcion.Text = "";
        }
        protected void lnkNuevo_Click(object sender, EventArgs e)
        {
            newClase();
        }

        protected void imgbNuevo_Click(object sender, ImageClickEventArgs e)
        {
            newClase();
        }

        protected void ImagecRegresar_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("urlEncry.aspx");
        }

        protected void imgcGuardar_Click(object sender, ImageClickEventArgs e)
        {
            if (Request["cod"] != null)
            {

                update();
            }
            else
            {
                save();
            }

        }

        protected void lnkcRegresar_Click(object sender, EventArgs e)
        {
            Response.Redirect("urlEncry.aspx");
        }

        protected void lnkcGuardar_Click(object sender, EventArgs e)
        {
            if (Request["cod"] != null)
            {

                update();
            }
            else
            {
                save();
            }
        }
    }
}