﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ImagenPrevisuali.aspx.cs" Inherits="MatriculacionVehicular.Public.ImagenPrevisuali" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <title>Imagen</title>
</head>
<body>
    <script>
        function ImagenP(input) {
            if (input.files && input.files[0]) {
                var extension = input.files[0].name.split('.').pop().toLowerCase();
                var ImageDir = new FileReader();
                ImageDir.onload = function (e) {
                    if (extension == 'pdf') {
                        $("#imagen1").attr('src', '');
                        $("#imagen2").attr('src', e.target.result);
                        $("#imagen2").attr('Width', '400px');
                        $("#imagen2").attr('Height', '400px');

                    }
                    if (extension == 'png' || extension == 'jpg' || extension == 'jpeg') {
                        $("#imagen1").attr('src', e.target.result);
                        $("#imagen2").attr('src', '');
                        $("#imagen2").attr('Width', '0px');
                        $("#imagen2").attr('Height', '0px');
                    }
                }
                ImageDir.readAsDataURL(input.files[0]);
            }
        }
    </script>
    <form id="form1" runat="server">
        <div>
            <asp:Image ID="imagen1" runat="server" Width="400px" />
            <iframe    id="imagen2" runat="server" width="0" style="border:none"></iframe>
            <asp:FileUpload ID="Imagen3" runat="server" onchange="ImagenP(this)" />
        </div>
    </form>
</body>
</html>
