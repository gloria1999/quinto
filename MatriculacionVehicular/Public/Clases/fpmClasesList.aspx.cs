﻿using MatriculacionVehicular.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MatriculacionVehicular.userControl;

namespace MatriculacionVehicular.Public.Clases
{
    public partial class fpmClasesList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                loadClase();
                //InitializeComponent();
            }
        }
        //private void InitializeComponent()
        //{
        //    this.Load += new System.EventHandler(this.loadClase);
        //}

        private void loadClase()
        {
            try
            {
                var taskListClase = Task.Run(() => Logica.LogicaClase.getClases());
                taskListClase.Wait();
                var listClase = taskListClase.Result;
                if (listClase.Count > 0 && listClase != null)
                {
                    //this.Load += new System.EventHandler(this.loadClase);
                    //UC_Clases1.E
                    UC_Clases1.GridView.DataSource = listClase.Select(data => new                    
                    {
                        ID = data.cla_id,
                        DESCRIPCION = data.cla_descripcion,
                        IMAGEN= data.cla_imagen,
                        FECHA = data.cla_add,
                        ESTADO = data.cla_status
                    }).ToList();
                    UC_Clases1.GridView.DataBind();

                }
            }
            catch (Exception ex)
                {
            }

        
        }
        


               
        protected void UC_Clases1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                string codigoClase = Convert.ToString(e.CommandArgument);
                if (e.CommandName.Equals("Modificar"))
                {
                    

                    //ENCRIPTAR EN TIPO HASH O OTRO ESO SE TIENE COMO MOSTRAR
                    //JUNTO CON LA URL EL codigoClase de la URL en string encrptado minuto 12.44
                    // string fj= txt
                    Response.Redirect("fpmClaseNuevo.aspx?cod=" + codigoClase);
                }
                else if (e.CommandName.Equals("Eliminar"))
                {
                    Clase dataclase = new Clase();
                    var taskclase = Task.Run(() => Logica.LogicaClase.getClasexId(int.Parse(codigoClase)));
                    taskclase.Wait();
                    dataclase = taskclase.Result;
                    if (dataclase != null)
                    {
                        var taskdelete = Task.Run(() => Logica.LogicaClase.deleteClase(dataclase));
                        taskdelete.Wait();
                        if (taskdelete.Result)
                        {
                            loadClase();
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        //    protected void UC_Clases1_RowCommand(object sender, GridViewCommandEventArgs e)
        //{

        //        string codigoClase = Convert.ToString(e.CommandArgument);
        //        if (e.CommandName=="Modificar")
        //        {
        //            //ENCRIPTAR EN TIPO HASH O OTRO ESO SE TIENE COMO MOSTRAR
        //            //JUNTO CON LA URL EL codigoClase de la URL en string encrptado minuto 12.44
        //            // string fj= txt
        //            Response.Redirect("fpmClaseNuevo.aspx?cod=" + codigoClase);
        //        }
        //        else if (e.CommandName == "Eliminar")
        //        {
        //            try
        //            {
        //                Clase dataclase = new Clase();
        //                var taskclase = Task.Run(() => Logica.LogicaClase.getClasexId(int.Parse(codigoClase)));
        //                taskclase.Wait();
        //                dataclase = taskclase.Result;
        //                if (dataclase != null)
        //                {
        //                    var taskdelete = Task.Run(() => Logica.LogicaClase.deleteClase(dataclase));
        //                    taskdelete.Wait();
        //                    if (taskdelete.Result)
        //                    {
        //                        loadClase();
        //                    }
        //                }
        //            }
        //            catch (Exception ex)
        //            {

        //                throw;
        //            }
        //        }
        //    }

        protected void imgModificar_Click(object sender, ImageClickEventArgs e)
        {
            
            nuevoClase();
        }

        protected void imgEliminar_Click(object sender, ImageClickEventArgs e)
        {

        }
        private void nuevoClase()
        {
            Response.Redirect("fpmClaseNuevo.aspx");
        }

        protected void imgcNuevo_Click(object sender, ImageClickEventArgs e)
        {
            nuevoClase();

        }

        protected void lnkcNuevo_Click(object sender, EventArgs e)
        {
            nuevoClase();
        }
    }
}