﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="fpmClasesList.aspx.cs" Inherits="MatriculacionVehicular.Public.Clases.fpmClasesList" %>
<%@ Register Src="~/userControl/ucGridViewGeneral.ascx" TagPrefix="UC1" TagName="UC_Clases"%>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
     <div align="center" width="95%">
        <table>
            <tr>
                <td>
                    <h3>Lista Clases</h3>
                </td>
            </tr>
            <tr>
                <td>&nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:ImageButton ID="imgcNuevo" ImageUrl="~/images/icon_nuevo.png" runat="server" Width="32px" Height="32px" OnClick="imgcNuevo_Click"/>
                    <asp:LinkButton ID="lnkcNuevo" ImageUrl="~/images/icon_nuevo.png" runat="server" OnClick="lnkcNuevo_Click">Nuevo</asp:LinkButton>
                </td>
            </tr>
            <tr>
                <td>&nbsp;
                </td>
            </tr>
            <tr>
                <td align="center">
                 <UC1:UC_Clases ID="UC_Clases1"  runat="server"/>                    
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
