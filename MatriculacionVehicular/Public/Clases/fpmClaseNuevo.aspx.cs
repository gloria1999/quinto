﻿using MatriculacionVehicular.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MatriculacionVehicular.Public.Clases
{
    public partial class fpmClaseNuevo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request["cod"] != null)
                {                             
                    //DESENCRIPTAMOS LA URL PARA ENVIAR EL CODIGO NORMAL A LA BD
                    loadClase(int.Parse(Request["cod"].ToString()));
                }
            }

        }
        private void loadClase(int codigoProducto)
        {
            try
            {
                Clase dataProducto = new Clase();
                var tasProducto = Task.Run(() => Logica.LogicaClase.getClasexId(codigoProducto));
                tasProducto.Wait();
                dataProducto = tasProducto.Result;
                if (dataProducto != null)
                {
                    lblId.Text = dataProducto.cla_id.ToString();
                    txtDescripcion.Text = dataProducto.cla_descripcion;
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;

            }
        }
        private void newClase()
        {
            lblId.Text = "";
            txtDescripcion.Text = "";
        }

        private void save()
        {
            try
            {
                Clase dataProducto = new Clase();
                dataProducto.cla_descripcion = txtDescripcion.Text;
                var taskSave = Task.Run(() => Logica.LogicaClase.saveClase(dataProducto));
                taskSave.Wait();
                if (taskSave.Result)
                {
                    lblMessage.Text = "clase guardado correctamente";
                    newClase();
                    Response.Redirect("fpmClasesList.aspx");
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
        }

        private void update()
        {
            try
            {
                Clase dataProducto = new Clase();
                var tasProducto = Task.Run(() => Logica.LogicaClase.getClasexId(int.Parse(lblId.Text)));
                tasProducto.Wait();
                dataProducto = tasProducto.Result;
                if (dataProducto != null)
                {
                    dataProducto.cla_descripcion = txtDescripcion.Text;
                    var taskSave = Task.Run(() => Logica.LogicaClase.updateClase(dataProducto));
                    taskSave.Wait();
                    if (taskSave.Result)
                    {
                        lblMessage.Text = "Clase guardado correctamente";
                        newClase();
                        Response.Redirect("fpmClasesList.aspx");
                    }
                }


            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
        }

        protected void imgcNuevo_Click(object sender, ImageClickEventArgs e)
        {
            newClase();
        }

        protected void lnkNuevo_Click(object sender, EventArgs e)
        {
            newClase();
        }
        protected void imgcGuardar_Click(object sender, ImageClickEventArgs e)
        {
            if (Request["cod"] != null)
            {

                update();
            }
            else
            {
                save();
            }
        }
        protected void lnkcGuardar_Click(object sender, EventArgs e)
        {
            if (Request["cod"] != null)
            {

                update();
            }
            else
            {
                save();
            }
        }
        private void regresar()
        {
            Response.Redirect("fpmClasesList.aspx");

        }
        protected void ImagecRegresar_Click(object sender, ImageClickEventArgs e)
        {
            regresar();
        }
        protected void lnkcRegresar_Click(object sender, EventArgs e)
        {
            regresar();
        }
        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("./ImagenPrevisuali.aspx");
        }
    }
}