﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="fpmModeloList.aspx.cs" Inherits="MatriculacionVehicular.Public.Modelos.fpmModeloList" %>
<%@ Register Src="~/userControl/ucGridViewGeneral.ascx" TagPrefix="UC2" TagName="UC_Modelos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
 <div align="center" width="95%">
        <table>
            <tr>
                <td>
                    <h3>Lista Modelo</h3>
                </td>
            </tr>
            <tr>
                <td>&nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:ImageButton ID="imgmNuevo" ImageUrl="~/images/icon_nuevo.png" runat="server" Width="32px" Height="32px" OnClick="imgmNuevo_Click"/>
                    <asp:LinkButton ID="lnkmNuevo" ImageUrl="~/images/icon_nuevo.png" runat="server" OnClick="lnkmNuevo_Click">Nuevo</asp:LinkButton>
                </td>
            </tr>
            <tr>
                <td>&nbsp;
                </td>
            </tr>            

            <tr>
                <td align="center">
                    <UC2:UC_Modelos ID="UC_Modelos1"  runat="server"/>                    
                </td>
            </tr>


        </table>
    </div>


</asp:Content>