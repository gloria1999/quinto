﻿using MatriculacionVehicular.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MatriculacionVehicular.Public.Modelos
{
    public partial class fpmModeloList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                loadModelo();
            }

        }
        private void loadModelo()
        {
            try
            {
                var taskListClase = Task.Run(() => Logica.LogicaModelos.getModelo());
                taskListClase.Wait();

                var listClase = taskListClase.Result;

                if (listClase.Count > 0 && listClase != null)
                {
                    // UC_Clases1.GridView.DataSource=
                    UC_Modelos1.GridView.DataSource = listClase.Select(data => new
                    {
                        ID = data.modelo_id,
                        NOMBRE = data.mod_descripcion,
                        ESTADO = data.mod_status
                    }).ToList();
                    UC_Modelos1.DataBind();

                }
            }
            catch (Exception ex)
            {
            }

        }
        protected void UC_Modelos1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                string codigo = Convert.ToString(e.CommandArgument);
                if (e.CommandName.Equals("Modificar"))
                {
                    Response.Redirect("fpmModeloNuevo.aspx?cod=" + codigo);
                }
                else if (e.CommandName.Equals("Eliminar"))
                {
                    Modelo dataVehiculo = new Modelo();
                    var taskVehiculo = Task.Run(() => Logica.LogicaModelos.getModeloxId(int.Parse(codigo)));
                    taskVehiculo.Wait();
                    dataVehiculo = taskVehiculo.Result;
                    if (dataVehiculo != null)
                    {
                        var taskDelete = Task.Run(() => Logica.LogicaModelos.deleteModelo(dataVehiculo));
                        taskDelete.Wait();
                        if (taskDelete.Result)
                        {
                            loadModelo();
                        }
                    }


                }


            }
            catch (Exception)
            {

                throw;
            }
        }
        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                string codigo = Convert.ToString(e.CommandArgument);
                if (e.CommandName.Equals("Modificar"))
                {
                    Response.Redirect("fpmModeloNuevo.aspx?cod=" + codigo);
                }
                else if (e.CommandName.Equals("Eliminar"))
                {
                    Modelo dataVehiculo = new Modelo();
                    var taskVehiculo = Task.Run(() => Logica.LogicaModelos.getModeloxId(int.Parse(codigo)));
                    taskVehiculo.Wait();
                    dataVehiculo = taskVehiculo.Result;
                    if (dataVehiculo != null)
                    {
                        var taskDelete = Task.Run(() => Logica.LogicaModelos.deleteModelo(dataVehiculo));
                        taskDelete.Wait();
                        if (taskDelete.Result)
                        {
                            loadModelo();
                        }
                    }


                }


            }
            catch (Exception)
            {

                throw;
            }
        }
        private void nuevoClase()
        {
            Response.Redirect("fpmModeloNuevo.aspx");
        }
        protected void imgmNuevo_Click(object sender, ImageClickEventArgs e)
        {
            nuevoClase();
        }

        protected void lnkmNuevo_Click(object sender, EventArgs e)
        {
            nuevoClase();
        }
    }
}