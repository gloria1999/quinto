﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="fpmModeloNuevo.aspx.cs" Inherits="MatriculacionVehicular.Public.Modelos.fpmModeloNuevo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <table>
            <tr>
                <td colspan="2">
                    <h3><strong>Modelo Nuevo</strong> </h3>
                </td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table width="75" style="height:35px">
                        <tr>
                            <td>
                                <asp:imageButton ID="imgmnuevo" ImageUrl="~/images/icon_nuevo.png" runat="server" Width="32px" Height="32px" CausesValidation="false" OnClick="imgmnuevo_Click"/>
                                <asp:linkButton ID="lnkmnuevo" CausesValidation="false" runat="server" OnClick="lnkmnuevo_Click">NUEVO</asp:linkButton>
                           
                                <td>
                                
                                <asp:ImageButton ID="imgmGuardar" ImageUrl="~/images/icon_guardar.png" runat="server" Width="32px" Height="32px" OnClick="imgmGuardar_Click"/>
                                <asp:LinkButton ID="lnkmGuardar" runat="server" OnClick="lnkmGuardar_Click">GUARDAR</asp:LinkButton>
                            </td>
                            <td>
                                <asp:ImageButton ID="ImagemRegresar" ImageUrl="~/images/regresar.png" runat="server" Width="32px" Height="32px" CausesValidation="false" OnClick="ImagemRegresar_Click" />
                                <asp:LinkButton ID="lnkmRegresar" CausesValidation="false" runat="server" OnClick="lnkmRegresar_Click">REGRESAR</asp:LinkButton>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label10" runat="server" Text="ID"></asp:Label></td>
                <td>
                    <asp:Label ID="lblId" runat="server" Text=""></asp:Label></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label1" runat="server" Text="DESCRIPCION"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtDescripcion" runat="server" ControlToValidate="txtDescripcion" ></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Descripcion Campo Obligatorio" ControlToValidate="txtDescripcion" ForeColor="#CC0000" Text="*"></asp:RequiredFieldValidator>
                </td>
            </tr>
           
            <tr>
                <td>
                    <asp:Label ID="Label11" runat="server" Text=" Mensaje"></asp:Label>
                </td>
                <td>
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="true" ForeColor="#CC0000" />
                    <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            </table>


</asp:Content>
