﻿using MatriculacionVehicular.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MatriculacionVehicular.Public.Modelos
{
    public partial class fpmModeloNuevo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request["cod"] != null)
                {
                    //DESENCRIPTAMOS LA URL PARA ENVIAR EL CODIGO NORMAL A LA BD
                    loadModelo(int.Parse(Request["cod"].ToString()));
                }
            }

        }

        private void loadModelo(int codigoProducto)
        {
            try
            {
                Modelo dataProducto = new Modelo();
                var tasProducto = Task.Run(() => Logica.LogicaModelos.getModeloxId(codigoProducto));
                tasProducto.Wait();
                dataProducto = tasProducto.Result;
                if (dataProducto != null)
                {
                    lblId.Text = dataProducto.modelo_id.ToString();
                    txtDescripcion.Text = dataProducto.mod_descripcion;
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;

            }
        }

        private void newModelo()
        {
            lblId.Text = "";
            txtDescripcion.Text = "";
        }

        private void save()
        {
            try
            {
                Modelo dataProducto = new Modelo();
                dataProducto.mod_descripcion = txtDescripcion.Text;
                var taskSave = Task.Run(() => Logica.LogicaModelos.saveModelo(dataProducto));
                taskSave.Wait();
                if (taskSave.Result)
                {
                    lblMessage.Text = "modelo guardado correctamente";
                    newModelo();
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
        }

        private void update()
        {
            try
            {
                Modelo dataProducto = new Modelo();
                var tasProducto = Task.Run(() => Logica.LogicaModelos.getModeloxId(int.Parse(lblId.Text)));
                tasProducto.Wait();
                dataProducto = tasProducto.Result;
                if (dataProducto != null)
                {
                    dataProducto.mod_descripcion = txtDescripcion.Text;
                    var taskSave = Task.Run(() => Logica.LogicaModelos.updateModelo(dataProducto));
                    taskSave.Wait();
                    if (taskSave.Result)
                    {
                        lblMessage.Text = "Modelo guardado correctamente";
                        newModelo();
                    }
                }


            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
        }

        protected void imgmnuevo_Click(object sender, ImageClickEventArgs e)
        {
            newModelo();
        }

        protected void lnkmnuevo_Click(object sender, EventArgs e)
        {
            newModelo();
        }

        protected void imgmGuardar_Click(object sender, ImageClickEventArgs e)
        {
            if (Request["cod"] != null)
            {

                update();
            }
            else
            {
                save();
            }

        }

        protected void lnkmGuardar_Click(object sender, EventArgs e)
        {
            if (Request["cod"] != null)
            {

                update();
            }
            else
            {
                save();
            }

        }
        private void regresar()
        {
            Response.Redirect("fpmModeloList.aspx");

        }

        protected void ImagemRegresar_Click(object sender, ImageClickEventArgs e)
        {
            regresar();
        }

        protected void lnkmRegresar_Click(object sender, EventArgs e)
        {
            regresar();
        }
    }
}