﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vehiculo_API.Model;

namespace Vehiculo_API.Persistencia
{
    public class ContextoDatos : DbContext
    {
        public ContextoDatos(DbContextOptions<ContextoDatos> options) : base(options) { }
        public DbSet<Usuario> usuarios { get; set; }
        public DbSet<Rol> rols { get; set; }


        //1. Vamos a VER luego clic en Terminal para ejecutar cambios en la base de datos
        //dotnet ef nigrations add MigracionPostgresInicial --project Vehiculo_API
        //dotnet ef database update --project Vehiculo_API    (sirve para que se generen las tablas en postgres
    }
}
