﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Vehiculo_API.Model
{
    public class Rol
    {
        [Key]
        public int Idrol { get; set; }
        public string Descripcion { get; set; }
        public List<Usuario> Usuario { get; set; }
    }
}
