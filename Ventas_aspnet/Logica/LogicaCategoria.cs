﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Ventas_aspnet.Models;

namespace Ventas_aspnet.Logica
{
    public class LogicaCategoria
    {
        private static TBL_CARRITOEntities db = new TBL_CARRITOEntities();
        public static async Task<List<TBL_CATEGORIA>> getAllCategoria()
        {
            try
            {
                return await db.TBL_CATEGORIA.Where(data => data.cat_status.Equals("A")).ToListAsync();
            }
            catch (Exception ex)

            {
                throw new ArgumentException("Error al obtener Producto");
            }

        }
    }
}