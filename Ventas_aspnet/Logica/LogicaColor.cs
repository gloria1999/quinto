﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Ventas_aspnet.Models;

namespace Ventas_aspnet.Logica
{
    public class LogicaColor
    {
        private static BDMATRICULAEntities db = new BDMATRICULAEntities();
        public static async Task<List<Color>> getColor()
        {
            try
            {
                return await db.Color.Where(data => data.col_status =="A").ToListAsync();
            }
            catch (Exception ex)

            {
                throw new ArgumentException("Error al obtener color");
            }
        }

        public static async Task<Color> getColorxId(int idColor)
        {
            try
            {
                return await db.Color.FirstOrDefaultAsync(data => data.col_status=="A"
                        && data.col_id.Equals(idColor));
            }
            catch (Exception ex)

            {
                throw new ArgumentException("Error al obtener color");
            }

        }
        public static async Task<bool> saveColor(Color dataColor)
        {
            try
            {

                bool result = false;

                dataColor.col_status = "A";
                dataColor.col_add = DateTime.Now;
                db.Color.Add(dataColor);
                // dataProducto.Add(dataProducto);
                result=await db.SaveChangesAsync()>0;
                //result = true;
                return result;
            }
            catch (Exception ex)

            {
                throw new ArgumentException("Error al guardar color");
            }

        }
        public static async Task<bool> updateColor(Color dataColor)
        {
            try
            {

                bool result = false;

                //dataProducto.pro_status = "A";
                dataColor.col_update = DateTime.Now;
                // db.TBL_PRODUCTO.Add(dataProducto);
                result= await db.SaveChangesAsync() > 0;
                // result = true;
                 return result;
            }
            catch (Exception ex)

            {
                throw new ArgumentException("Error al modificar color");
            }

        }
        public static async Task<bool> deleteColor(Color dataColor)
        {
            try
            {

                bool result = false;

                dataColor.col_status = "I";
                dataColor.col_delete = DateTime.Now;
                //db.TBL_PRODUCTO.Add(dataProducto);

                //result = await db.SaveChangesAsync() > 0;
                //eliminar datos de forma fisisca 
                //db.TBL_PRODUCTO.Remove(dataProducto);
                //commit hacia la base d edatos
                result= await db.SaveChangesAsync() > 0;
                //result = true;
                 return result;
            }
            catch (Exception ex)

            {
                throw new ArgumentException("Error al guardar color");
            }

        }
    }
}