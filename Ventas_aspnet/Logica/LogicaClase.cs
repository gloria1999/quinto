﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Ventas_aspnet.Models;

namespace Ventas_aspnet.Logica
{
    public class LogicaClase
    {
        private static BDMATRICULAEntities db = new BDMATRICULAEntities();
        public static async Task<List<Clase>> getClases()
        {
            try
            {
                return await db.Clase.Where(data => data.cla_status=="A").ToListAsync();
            }
            catch (Exception ex)

            {
                throw new ArgumentException("Error al obtener Clase");
            }
        }

        public static async Task<Clase> getClasexId(int idClase)
        {
            try
            {
                return await db.Clase.FirstOrDefaultAsync(data => data.cla_status=="A" 
                        && data.cla_id.Equals(idClase));
            }
            catch (Exception ex)

            {
                throw new ArgumentException("Error al obtener clase");
            }

        }

       
        public static async Task<bool> saveClase(Clase dataClase)
        {
            try
            {

                bool result = false;

                dataClase.cla_status = "A";
                dataClase.cla_add = DateTime.Now;
                db.Clase.Add(dataClase);
                // dataProducto.Add(dataProducto);
                result=await db.SaveChangesAsync()>0;
                //result = true;
                return result;
            }
            catch (Exception ex)

            {
                throw new ArgumentException("Error al guardar clase");
            }

        }
        public static async Task<bool> updateClase(Clase dataClase)
        {
            try
            {

                bool result = false;

                //dataProducto.pro_status = "A";
                dataClase.cla_update = DateTime.Now;
                // db.TBL_PRODUCTO.Add(dataProducto);
                result= await db.SaveChangesAsync() > 0;
                // result = true;
                 return result;
            }
            catch (Exception ex)

            {
                throw new ArgumentException("Error al modificar clase");
            }

        }
        public static async Task<bool> deleteClase(Clase dataClase)
        {
            try
            {

                bool result = false;

                dataClase.cla_status = "I";
                dataClase.cla_delete = DateTime.Now;
                //db.TBL_PRODUCTO.Add(dataProducto);

                //result = await db.SaveChangesAsync() > 0;
                //eliminar datos de forma fisisca 
                //db.TBL_PRODUCTO.Remove(dataProducto);
                //commit hacia la base d edatos
                result= await db.SaveChangesAsync() > 0;
                //result = true;
                 return result;
            }
            catch (Exception ex)

            {
                throw new ArgumentException("Error al guardar clase");
            }

        }
    }
}