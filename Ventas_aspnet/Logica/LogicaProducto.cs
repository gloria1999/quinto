﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Ventas_aspnet.Models;

namespace Ventas_aspnet.Logica
{
    public class LogicaProducto
    {
        private static TBL_CARRITOEntities db = new TBL_CARRITOEntities();
        public static async Task<List<TBL_PRODUCTO>> getProducto()
        {
            try
            {
                return await db.TBL_PRODUCTO.Where(data => data.pro_status =="A").ToListAsync();
            }
            catch (Exception ex)

            {
                throw new ArgumentException("Error al obtener Producto");
            }

        }
        public static async Task<TBL_PRODUCTO> getProductoxId(int idProducto)
        {
            try
            {
                return await db.TBL_PRODUCTO.FirstOrDefaultAsync(data => data.pro_status =="A"
                    && data.pro_id.Equals(idProducto));
            }
            catch (Exception ex)

            {
                throw new ArgumentException("Error al obtener producto");
            }

        }
        public static async Task<TBL_PRODUCTO> getProductoxCode(string codigoProducto)
        {
            try
            {
                return await db.TBL_PRODUCTO.FirstOrDefaultAsync(data => data.pro_status == "A"
                    && data.pro_codigo.Equals(codigoProducto));
            }
            catch (Exception ex)

            {
                throw new ArgumentException("Error al obtener producto");
            }

        }
        public static async Task<bool> saveProducto(TBL_PRODUCTO dataProducto)
        {
            try
            {

                bool result = false;

                dataProducto.pro_status = "A";
                dataProducto.pro_fechacreacion = DateTime.Now;
                db.TBL_PRODUCTO.Add(dataProducto);
                // dataProducto.Add(dataProducto);
                result = await db.SaveChangesAsync() > 0;
               // await db.SaveChangesAsync();
                //result = true;                   
                return result;
            }
            catch (Exception ex)

            {
                throw new ArgumentException("Error al guardar Producto");
            }

        }

        //public static async task<bool> saveproducto2(tbl_producto dataproducto1)
        //{
        //    try
        //    {
        //        bool result = false;

        //        dataproducto1.pro_status = "a";
        //        dataproducto1.pro_fechacreacion = datetime.now;

        //        // asyncrono
        //        //db.stpinsertproduct(producto.pro_codigo,)

        //        //commit hacia la base de datos
        //        await db.savechangesasync();
        //        result = true;
        //        return result;

        //    }
        //    catch (exception ex)
        //    {
        //        throw new argumentexception("error al guardar productos");
        //    }

        //}

        public static async Task<bool> updateProducto(TBL_PRODUCTO dataProducto)
        {
            try
            {

                bool result = false;

                //dataProducto.pro_status = "A";
                dataProducto.pro_fechacreacion = DateTime.Now;
                // db.TBL_PRODUCTO.Add(dataProducto);
                //return await db.SaveChangesAsync() > 0;
                result= await db.SaveChangesAsync() > 0;
                // result = true;
                 return result;
            }
            catch (Exception ex)

            {
                throw new ArgumentException("Error al modificar producto");
            }

        }

        public static async Task<bool>  deleteProducto(TBL_PRODUCTO dataProducto)
        {
            try
            {

                bool result = false;

                dataProducto.pro_status = "I";
                dataProducto.pro_fechacreacion = DateTime.Now;
                //db.TBL_PRODUCTO.Add(dataProducto);

                result = await db.SaveChangesAsync() > 0;
                //eliminar datos de forma fisisca 
                //db.TBL_PRODUCTO.Remove(dataProducto);
                //commit hacia la base d edatos
                //return await db.SaveChangesAsync() > 0;
                //result = true;
                return result;
            }
            catch (Exception ex)

            {
                throw new ArgumentException("Error al eliminar producto");
            }

        }


    }
}
