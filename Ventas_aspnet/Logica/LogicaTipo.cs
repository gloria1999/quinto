﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Ventas_aspnet.Models;

namespace Ventas_aspnet.Logica
{
    public class LogicaTipo
    {
        private static BDMATRICULAEntities db = new BDMATRICULAEntities();
        public static async Task<List<Tipo>> getTipo()
        {
            try
            {
                return await db.Tipo.Where(data => data.tip_status=="A").ToListAsync();
            }
            catch (Exception ex)

            {
                throw new ArgumentException("Error al obtener tipo");
            }
        }

        public static async Task<Tipo> getTipoxId(int idTipo)
        {
            try
            {
                return await db.Tipo.FirstOrDefaultAsync(data => data.tip_status=="A"
                        && data.tip_id.Equals(idTipo));
            }
            catch (Exception ex)

            {
                throw new ArgumentException("Error al obtener tipo");
            }

        }
        public static async Task<bool> saveTipo(Tipo dataTipo)
        {
            try
            {

                bool result = false;

                dataTipo.tip_status = "A";
                dataTipo.tip_add = DateTime.Now;
                db.Tipo.Add(dataTipo);
                // dataProducto.Add(dataProducto);
                result=await db.SaveChangesAsync()>0;
                ///result = true;
                return result;
            }
            catch (Exception ex)

            {
                throw new ArgumentException("Error al guardar tipo");
            }

        }
        public static async Task<bool> updateTipo(Tipo dataTipo)
        {
            try
            {

                bool result = false;

                //dataProducto.pro_status = "A";
                dataTipo.tip_add = DateTime.Now;
                // db.TBL_PRODUCTO.Add(dataProducto);
                result= await db.SaveChangesAsync() > 0;
                // result = true;
                 return result;
            }
            catch (Exception ex)

            {
                throw new ArgumentException("Error al modificar tipo");
            }

        }
        public static async Task<bool> deleteColor(Tipo dataTipo)
        {
            try
            {

                bool result = false;

                dataTipo.tip_status = "I";
                dataTipo.tip_add = DateTime.Now;
                //db.TBL_PRODUCTO.Add(dataProducto);

                //result = await db.SaveChangesAsync() > 0;
                //eliminar datos de forma fisisca 
                //db.TBL_PRODUCTO.Remove(dataProducto);
                //commit hacia la base d edatos
                result= await db.SaveChangesAsync() > 0;
                //result = true;
                 return result;
            }
            catch (Exception ex)

            {
                throw new ArgumentException("Error al guardar tipo");
            }

        }
    }
}