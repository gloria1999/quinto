﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ventas_aspnet.Models;
using Ventas_aspnet.UserControls;

namespace Ventas_aspnet.Public.Producto
{
    public partial class wfmProductoNuevo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request["cod"] != null)
                {
                    loadProducto(int.Parse(Request["cod"].ToString()));
                }
            }
        }
        private void loadProducto(int codigoProducto)
        {
            try
            {
                TBL_PRODUCTO dataProducto = new TBL_PRODUCTO();
                var tasProducto = Task.Run(() => Logica.LogicaProducto.getProductoxId(codigoProducto));
                tasProducto.Wait();
                dataProducto = tasProducto.Result;
                if (dataProducto != null)
                {
                    lblId.Text = dataProducto.pro_id.ToString();
                    txtCodigo.Text = dataProducto.pro_codigo;
                    UC_Categoria1.DropDownList.SelectedValue = dataProducto.cat_id.ToString();
                    txtNombre.Text = dataProducto.pro_nombre;
                    txtDescripcion.Text = dataProducto.pro_descripcion;
                    txtPrecioCompra.Text = dataProducto.pro_preciocompra.ToString("0.00");
                    txtPrecioVenta.Text = dataProducto.pro_precioventa.ToString("0.00");
                    txtStockMinimo.Text = dataProducto.pro_stockminimo.ToString();
                    txtStockMaximo.Text = dataProducto.pro_stockmaximo.ToString();
                    txtCodigo.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;

            }
        }

        private void newProduct()
        {
            lblId.Text = "";
            txtCodigo.Text = "";
            txtDescripcion.Text = "";
            txtNombre.Text = "";
            txtPrecioCompra.Text = "";
            txtPrecioVenta.Text = "";
            txtStockMaximo.Text = "";
            txtStockMinimo.Text = "";
            UC_Categoria1.DropDownList.SelectedIndex = 0;
            txtCodigo.Enabled = true; //bloquear la caja de texto codigo y tambien poner en load
        }

        private void save()
        {
            try
            {
                TBL_PRODUCTO dataProducto = new TBL_PRODUCTO();
                dataProducto.cat_id = Convert.ToInt16(UC_Categoria1.DropDownList.SelectedValue);
                dataProducto.pro_codigo = txtCodigo.Text;
                //validar si existe el codigo nuevo
                var productoExiste = Task.Run(() => Logica.LogicaProducto.getProductoxCode(dataProducto.pro_codigo)).Result;
                if (productoExiste !=null)
                {
                    lblMessage.Text = "El codigo ya se ha asignado a otro producto";
                    return;
                }
                dataProducto.pro_nombre = txtNombre.Text.ToUpper();
                dataProducto.pro_descripcion = txtDescripcion.Text.ToUpper();
                dataProducto.pro_stockminimo = int.Parse(txtStockMinimo.Text);
                dataProducto.pro_stockmaximo = int.Parse(txtStockMaximo.Text);
                dataProducto.pro_preciocompra = decimal.Parse(txtPrecioCompra.Text);
                dataProducto.pro_precioventa = decimal.Parse(txtPrecioVenta.Text);
                //Imagen
                if (fuImagenProducto.HasFile)
                {
                    try
                    {
                        if (fuImagenProducto.PostedFile.ContentType == "image/png" || fuImagenProducto.PostedFile.ContentType == "image/jpg")
                        {
                            if (fuImagenProducto.PostedFile.ContentLength < 1000000)
                            {
                                string nombreProducto = txtCodigo.Text + ".jpg";
                                fuImagenProducto.SaveAs(Server.MapPath("~/images/product/") + nombreProducto);
                            }
                            else
                            {
                                lblMessage.Text = "El tamanio de la imagen es de 100 kb";
                                return;
                            }
                        }
                        else
                        {
                            lblMessage.Text = "Solo se acepta imagenes de tipo png y jpg";
                            return;
                        }
                    }
                    catch (Exception)
                    {
                        lblMessage.Text = "Error al cargar la imagen";
                    }
                }

                dataProducto.pro_imagen = "~/images/product/" + txtCodigo.Text + ".jpg";
                var taskSave = Task.Run(() => Logica.LogicaProducto.saveProducto(dataProducto));
                taskSave.Wait();
                if (taskSave.Result)
                {
                    lblMessage.Text = "Producto guardado correctamente";
                    newProduct();
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
        }

        private void update()
        {
            try
            {
                TBL_PRODUCTO dataProducto = new TBL_PRODUCTO();
                var tasProducto = Task.Run(() => Logica.LogicaProducto.getProductoxId(int.Parse(lblId.Text)));
                tasProducto.Wait();
                dataProducto = tasProducto.Result;
                if (dataProducto != null)
                {
                    dataProducto.cat_id = Convert.ToInt16(UC_Categoria1.DropDownList.SelectedValue);
                    dataProducto.pro_codigo = txtCodigo.Text;
                    dataProducto.pro_nombre = txtNombre.Text.ToUpper();
                    dataProducto.pro_descripcion = txtDescripcion.Text.ToUpper();
                    dataProducto.pro_stockminimo = int.Parse(txtStockMinimo.Text);
                    dataProducto.pro_stockmaximo = int.Parse(txtStockMaximo.Text);
                    dataProducto.pro_preciocompra = decimal.Parse(txtPrecioCompra.Text);
                    dataProducto.pro_precioventa = decimal.Parse(txtPrecioVenta.Text);
                    //
                    //Imagen
                    if (fuImagenProducto.HasFile)
                    {
                        try
                        {
                            if (fuImagenProducto.PostedFile.ContentType == "image/png" || fuImagenProducto.PostedFile.ContentType == "image/jpg" || fuImagenProducto.PostedFile.ContentType == "image/jpeg")
                            {
                                if (fuImagenProducto.PostedFile.ContentLength < 1000000)
                                {
                                    string nombreProducto = txtCodigo.Text + ".jpg";
                                    fuImagenProducto.SaveAs(Server.MapPath("~/images/product/") + nombreProducto);
                                }
                                else
                                {
                                    lblMessage.Text="El tamanio de la imagen es de 100 kb";
                                    return;
                                }
                            }
                            else
                            {
                                lblMessage.Text="Solo se acepta imagenes de tipo png y jpg";
                                return;
                            }
                        }
                        catch (Exception)
                        {
                            lblMessage.Text= "Error al cargar la imagen";
                        }
                    }

                    dataProducto.pro_imagen = "~/images/product/" + txtCodigo.Text + ".jpg";
                    var taskSave = Task.Run(() => Logica.LogicaProducto.updateProducto(dataProducto));
                    taskSave.Wait();
                    if (taskSave.Result)
                    {
                        lblMessage.Text="Producto guardado correctamente";
                        newProduct();
                    }
                }


            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
        }



        protected void imgbGuardar_Click(object sender, ImageClickEventArgs e)
        {
            if (Request["cod"] != null)
            {

                update();
            }
            else
            {
                save();
            }
        }

        protected void lnkGuardar_Click(object sender, EventArgs e)
        {
            if (Request["cod"] != null)
            {
                update();
            }
            else
            {
                save();
            }
        }

        private void regresar()
        {
            Response.Redirect("wfmProductList.aspx");

        }
        protected void imgpnuevo_Click(object sender, ImageClickEventArgs e)
        {
            newProduct();
        }

        protected void lnknuevo_Click(object sender, EventArgs e)
        {
            newProduct();
        }

        protected void ImageRegresar_Click(object sender, ImageClickEventArgs e)
        {
            regresar();
        }

        protected void lnkRegresar_Click(object sender, EventArgs e)
        {
            regresar();
        }
    }

}