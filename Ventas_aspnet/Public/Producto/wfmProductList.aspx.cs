﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ventas_aspnet.Models;

namespace Ventas_aspnet.Public.Producto
{
    public partial class wfmProductList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                loadproducto();
            }

        }

        private void loadproducto()
        {
            try
            {
                var tasklistproducto = Task.Run(() => Logica.LogicaProducto.getProducto());
                tasklistproducto.Wait();

                var listproducto = tasklistproducto.Result;

                if (listproducto.Count > 0 && listproducto != null)
                {
                    UC_Producto1.GridView.DataSource = listproducto.Select(data => new
                    {
                        id = data.pro_id,
                        nombre = data.pro_nombre,
                        fecha_creacion = data.pro_fechacreacion,
                        estado = data.pro_status
                    }).ToList();
                    UC_Producto1.DataBind();

                }
            }
            catch (Exception ex)
            {
            }

        }


        protected void gdvDatosProducto_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            try
            {
                string codigo = Convert.ToString(e.CommandArgument);
                if (e.CommandName.Equals("Modificar"))
                {
                    Response.Redirect("wfmProductoNuevo.aspx?cod=" + codigo);
                }
                else if (e.CommandName.Equals("Eliminar"))
                {
                    TBL_PRODUCTO dataProducto = new TBL_PRODUCTO();
                    var taskProducto = Task.Run(() => Logica.LogicaProducto.getProductoxId(int.Parse(codigo)));
                    taskProducto.Wait();
                    dataProducto = taskProducto.Result;
                    if (dataProducto != null)
                    {
                        var taskDelete = Task.Run(() => Logica.LogicaProducto.deleteProducto(dataProducto));
                        taskDelete.Wait();
                        if (taskDelete.Result)
                        {
                             loadproducto();
                        }
                    }


                }


            }
            catch (Exception)
            {

                throw;
            }
        }

        private void nuevo()
        {
            Response.Redirect("wfmProductoNuevo.aspx");
        }

        protected void imgNuevo_Click(object sender, ImageClickEventArgs e)
        {
            nuevo();
        }
    }
}
















