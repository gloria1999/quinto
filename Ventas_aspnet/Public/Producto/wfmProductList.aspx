﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="wfmProductList.aspx.cs" Inherits="Ventas_aspnet.Public.Producto.wfmProductList" %>
<%@ Register Src="~/UserControls/ucVehiculoGridView.ascx" TagPrefix="UCP1" TagName="UC_Producto"   %> 
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div align="center" width="95%">
        <table>
            <tr>
                <td>
                    <h3>Lista Producto</h3>
                </td>
            </tr>
            <tr>
                <td>&nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:ImageButton ID="imgNuevo" ImageUrl="~/images/icon_nuevo.png" runat="server" Width="32px" Height="32px" OnClick="imgNuevo_Click"  />
                    <asp:LinkButton ID="lnkNuevo" ImageUrl="~/images/icon_nuevo.png" runat="server"  >Nuevo</asp:LinkButton>
                </td>
            </tr>
            <tr>
                <td>&nbsp;
                </td>
            </tr>
                  <tr>
                <td align="center"> 
                    <UCP1:UC_Producto ID="UC_Producto1" runat="server"  />
                    <%--<UCV1:UC ID="UC_Producto1" />--%>
                </td>
            </tr>
           
        </table>
    </div>


</asp:Content>