﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="wfmClasesList.aspx.cs" Inherits="Ventas_aspnet.Public.Clases.wfmClasesList" %>
<%--<%@ Register Src="~/UserControls/wfmGridView.ascx" TagName="UC_GridView" TagPrefix="UCGV1"%>--%>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div align="center" width="95%">
        <table>
            <tr>
                <td>
                    <h3>Lista Clase </h3>
                </td>
            </tr>
            <tr>
                <td>&nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:ImageButton ID="imgcNuevo" ImageUrl="~/images/icon_nuevo.png" runat="server" Width="32px" Height="32px" OnClick="imgcNuevo_Click1"   />
                    <asp:LinkButton ID="lnkcNuevo" ImageUrl="~/images/icon_nuevo.png" runat="server" OnClick="lnkcNuevo_Click1"   >Nuevo</asp:LinkButton>
                </td>
            </tr>
            <tr>
                <td>&nbsp;
                </td>
            </tr>

      

          <tr>
                <td align="center">
                    <asp:GridView ID="gdvDatosClase" runat="server" CellPadding="2"  BackColor="#DEBA84" BorderColor="#DEBA84" BorderStyle="None" BorderWidth="1px" CellSpacing="2" OnRowCommand="gdvDatosClase_RowCommand" >
                   <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:imageButton ID="imgvModificar"  imageurl="~/images/modificar.png" runat="server" Width="32px" Height="32px" CommandName="Modificar" CommandArgument='<%#Eval("ID") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:imageButton ID="imgvEliminar" imageurl="~/images/icon_delete.png" runat="server" Width="32px" Height="32px" CommandName="Eliminar" CommandArgument='<%#Eval("ID") %>' OnClientClick="return confirm('En realidad desea eliminar registro?')" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                     
                    <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                    <HeaderStyle BackColor="#A55129" Font-Bold="true" ForeColor="White" />
                    <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
                        <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
                        <SelectedRowStyle BackColor="#738A9C" Font-Bold="true" ForeColor="White"/>
                        <SortedAscendingCellStyle BackColor="#FFF1D4" />
                        <SortedAscendingHeaderStyle BackColor="#B95C30"    />
                        <SortedDescendingCellStyle BackColor="#F1E5CE"  />
                        <SortedDescendingHeaderStyle BackColor="#93451F"  />
                        </asp:GridView>
       
                </td>
            </tr>
        </table>
    </div>
</asp:Content>