﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="wfmClasesNuevo.aspx.cs" Inherits="Ventas_aspnet.Public.Clases.wfmClasesNuevo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <table>
            <tr>
                <td colspan="2">
                    <h3><strong>Clase Nuevo</strong> </h3>
                </td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;
                </td> 
            </tr>
            <tr>  
                <td colspan="2">
                    <table width="75" style="height:35px">
                        <tr>
                            <td>
                                <asp:ImageButton ID="imgbNuevo" ImageUrl="~/images/icon_nuevo.png" runat="server" Width="32px" Height="32px" CausesValidation="false" OnClick="imgbNuevo_Click"  />
                                <asp:LinkButton ID="lnkNuevo" CausesValidation="false" runat="server" OnClick="lnkNuevo_Click">Nuevo</asp:LinkButton>
                           
                                <td>
                                
                                <asp:ImageButton ID="imgcGuardar" ImageUrl="~/images/icon_guardar.png" runat="server" Width="32px" Height="32px" OnClick="imgcGuardar_Click"/>
                                <asp:LinkButton ID="lnkcGuardar" runat="server" OnClick="lnkcGuardar_Click">Guardar</asp:LinkButton>
                            </td>
                            <td>
                                <asp:ImageButton ID="ImagecRegresar" ImageUrl="~/images/regresar.png" runat="server" Width="32px" Height="32px" CausesValidation="false" OnClick="ImagecRegresar_Click"/>
                                <asp:LinkButton ID="lnkcRegresar" CausesValidation="false" runat="server" OnClick="lnkcRegresar_Click"  >Regresar</asp:LinkButton>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label10" runat="server" Text="Id"></asp:Label></td>
                <td>
                    <asp:Label ID="lblId" runat="server" Text=""></asp:Label></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label1" runat="server" Text="Descripcion"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtDescripcion" runat="server" ControlToValidate="txtDescripcion" TextMode="MultiLine" ></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Descripcion Campo Obligatorio" ControlToValidate="txtDescripcion" ForeColor="#CC0000" Text="*">*</asp:RequiredFieldValidator>
                </td>
            </tr>
           
           
           
            <tr>
                <td>
                    <asp:Label ID="Label11" runat="server" Text=" Mensaje"></asp:Label>
                </td>
                <td>
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="true" ForeColor="#CC0000" />
                    <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            </table>





</asp:Content>