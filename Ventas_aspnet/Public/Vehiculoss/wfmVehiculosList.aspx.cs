﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ventas_aspnet.Models;

namespace Ventas_aspnet.Public.Vehiculoss
{
    public partial class wfmVehiculosList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                loadVehiculo();
            }
        }

        private void loadVehiculo()
        {
            try
            {
                var taskListVehiculo = Task.Run(() => Logica.LogicaVehiculo.getVehiculo());
                taskListVehiculo.Wait();
                var listVehiculo = taskListVehiculo.Result;
                if (listVehiculo.Count > 0 && listVehiculo != null)
                {
                    UC_GridVehiculo1.GridView.DataSource=listVehiculo.Select(data =>new
                    //gdvDatosVehiculos.DataSource = listVehiculo.Select(data => new
                    {
                        ID = data.veh_id,
                        PLACAANTERIOR = data.veh_placaanterior,
                        PLACAACTUAL = data.veh_placaactual,
                        CHASIS = data.veh_chasis,
                        MOTOR = data.veh_motor,
                        TONELAJE = data.veh_tonelaje.ToString("0.00"),
                        FECHACOMPRA = data.veh_fechacompra,
                        FECHA_CREACION = data.veh_add,
                        ANIO = data.vehiculo_anio,
                        CLASE = data.Clase.cla_descripcion,
                        MODELO = data.Modelo.mod_descripcion,
                        TIPO = data.Tipo.tip_descripcion,
                        COLOR = data.Color.col_nombre,
                        PAIS = data.Pais.pai_nombre,
                        ESTADO = data.veh_status
                    }).ToList();
                    UC_GridVehiculo1.GridView.DataBind();
                   // gdvDatosVehiculos.DataBind();
                }
            }
            catch (Exception ex)
            {
            }

        }

        protected void gdvDatosVehiculos_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                string codigoVehiculo = Convert.ToString(e.CommandArgument);
                if (e.CommandName.Equals("Modificar"))
                {
                    Response.Redirect("wfmVehiculoNuevo.aspx?cod=" + codigoVehiculo);
                }
                else if (e.CommandName.Equals("Eliminar"))
                {
                    Vehiculo dataVehiculo = new Vehiculo();
                    var taskVehiculo = Task.Run(() => Logica.LogicaVehiculo.getVehiculoxId(int.Parse(codigoVehiculo)));
                    taskVehiculo.Wait();
                    dataVehiculo = taskVehiculo.Result;
                    if (dataVehiculo != null)
                    {
                        var taskDelete = Task.Run(() => Logica.LogicaVehiculo.deleteVehiculo(dataVehiculo));
                        taskDelete.Wait();
                        if (taskDelete.Result)
                        {
                            loadVehiculo();
                        }
                    }


                }


            }
            catch (Exception)
            {

                throw;
            }
        }
        private void nuevoVehiculo()
        {
            Response.Redirect("wfmVehiculoNuevo.aspx");
        }
        protected void imgvNuevo_Click(object sender, ImageClickEventArgs e)
        {
            nuevoVehiculo();
        }

        protected void lnkvNuevo_Click(object sender, EventArgs e)
        {
            nuevoVehiculo();
        }
    }
}