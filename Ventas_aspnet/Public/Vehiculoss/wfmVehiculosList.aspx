﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="wfmVehiculosList.aspx.cs" Inherits="Ventas_aspnet.Public.Vehiculoss.wfmVehiculosList" %>
<%@ Register Src="~/UserControls/ucVehiculoGridView.ascx" TagName="UC_GridVehiculo" TagPrefix="UCGVV1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
     <div align="center" width="95%">
        <table>
            <tr>
                <td>
                    <h3>Lista Vehiculos</h3>
                </td>
            </tr>
            <tr>
                <td>&nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:ImageButton ID="imgvNuevo" ImageUrl="~/images/icon_nuevo.png" runat="server" Width="32px" Height="32px" OnClick="imgvNuevo_Click"  />
                    <asp:LinkButton ID="lnkvNuevo" ImageUrl="~/images/icon_nuevo.png" runat="server" OnClick="lnkvNuevo_Click"  >Nuevo</asp:LinkButton>
                </td>
            </tr>
            <tr>
                <td>&nbsp;
                </td>
            </tr> 
            <tr>
                <td align="center">
                 <UCGVV1:UC_GridVehiculo ID="UC_GridVehiculo1" runat="server" />                 
                </td>
            </tr>
        </table>
    </div>
</asp:Content>