﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.UI;
using Ventas_aspnet.Models;

namespace Ventas_aspnet.Public.Vehiculoss
{
    public partial class wfmVehiculoNuevo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request["cod"] != null)
                {
                    loadVehiculo(int.Parse(Request["cod"].ToString()));
                }
                //UcCargarDatosClase();
            }
            
        }
        
        private void loadVehiculo(int codigoVehiculo)
        {
            try
            {
                Vehiculo dataVehiculo = new Vehiculo();
                var tasVehiculo = Task.Run(() => Logica.LogicaVehiculo.getVehiculoxId(codigoVehiculo));
                tasVehiculo.Wait();
                dataVehiculo = tasVehiculo.Result;
                if (dataVehiculo != null)
                {
                    lblId.Text = dataVehiculo.veh_id.ToString();                    
                    txtPlacaAnterior.Text = dataVehiculo.veh_placaanterior;
                    txtPlacaActual.Text = dataVehiculo.veh_placaactual;
                    txtChasis.Text = dataVehiculo.veh_chasis;
                    txtMotor.Text = dataVehiculo.veh_motor;
                    txtTonelaje.Text = dataVehiculo.veh_tonelaje.ToString("0.00");
                    txtAnio.Text = dataVehiculo.vehiculo_anio.ToString();
                    UC_Clase1.DropDownList.SelectedValue = dataVehiculo.cla_id.ToString();
                    UC_Modelo4.DropDownList.SelectedValue = dataVehiculo.modelo_id.ToString();
                    UC_Tipo5.DropDownList.SelectedValue = dataVehiculo.tip_id.ToString();
                    UC_Color3.DropDownList.SelectedValue = dataVehiculo.col_id.ToString();
                    UC_Pais6.DropDownList.SelectedValue = dataVehiculo.pai_id.ToString();
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;

            }
        }
        private void newVehiculo()
        {
            lblId.Text                    = "";
            txtPlacaAnterior.Text         = "";
            txtPlacaActual.Text           = "";
            txtChasis.Text                = "";
            txtMotor.Text                 = "";
            txtTonelaje.Text              = "";
            txtAnio.Text                  = "";
            UC_Clase1.DropDownList.SelectedIndex  = 0;
            UC_Modelo4.DropDownList.SelectedIndex = 0;
            UC_Tipo5.DropDownList.SelectedIndex   = 0;
            UC_Color3.DropDownList.SelectedIndex  = 0;
            UC_Pais6.DropDownList.SelectedIndex   = 0;
        }
        private void saveVehiculo()
        {
            try
            {
                Vehiculo dataVehiculo = new Vehiculo();
                dataVehiculo.cla_id    = Convert.ToInt16(UC_Clase1.DropDownList.SelectedValue);
                dataVehiculo.modelo_id = Convert.ToInt16(UC_Modelo4.DropDownList.SelectedValue);
                dataVehiculo.tip_id    = Convert.ToInt16(UC_Tipo5.DropDownList.SelectedValue);
                dataVehiculo.col_id    = Convert.ToInt16(UC_Color3.DropDownList.SelectedValue);
                dataVehiculo.pai_id    = Convert.ToInt16(UC_Pais6.DropDownList.SelectedValue);
                dataVehiculo.veh_placaanterior = txtPlacaAnterior.Text;
                dataVehiculo.veh_placaactual   = txtPlacaActual.Text;
                dataVehiculo.veh_chasis        = txtChasis.Text;
                dataVehiculo.veh_motor         = txtMotor.Text;
                dataVehiculo.veh_tonelaje      = decimal.Parse(txtTonelaje.Text);
                dataVehiculo.vehiculo_anio     = Convert.ToInt16(txtAnio.Text);
                var taskSave = Task.Run(() => Logica.LogicaVehiculo.saveVehiculo(dataVehiculo));
                taskSave.Wait();
                if (taskSave.Result)
                {
                    lblMessage.Text = "Vehiculo guardado correctamente";
                    newVehiculo();
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
        }

        private void updateVehiculo()
        {
            try
            {
                Vehiculo dataVehiculo = new Vehiculo();
                var tasVehiculo = Task.Run(() => Logica.LogicaVehiculo.getVehiculoxId(int.Parse(lblId.Text)));
                tasVehiculo.Wait();
                dataVehiculo = tasVehiculo.Result;
                if (dataVehiculo != null)
                {
                    dataVehiculo.cla_id    = Convert.ToInt16(UC_Clase1.DropDownList.SelectedValue);
                    dataVehiculo.modelo_id = Convert.ToInt16(UC_Modelo4.DropDownList.SelectedValue);
                    dataVehiculo.tip_id    = Convert.ToInt16(UC_Tipo5.DropDownList.SelectedValue);
                    dataVehiculo.col_id    = Convert.ToInt16(UC_Color3.DropDownList.SelectedValue);
                    dataVehiculo.pai_id    = Convert.ToInt16(UC_Pais6.DropDownList.SelectedValue);
                    dataVehiculo.veh_placaanterior = txtPlacaAnterior.Text;
                    dataVehiculo.veh_placaactual   = txtPlacaActual.Text;
                    dataVehiculo.veh_chasis        = txtChasis.Text;
                    dataVehiculo.veh_motor         = txtMotor.Text;
                    dataVehiculo.veh_tonelaje      = decimal.Parse(txtTonelaje.Text);
                    dataVehiculo.vehiculo_anio     = Convert.ToInt16(txtAnio.Text);
                    var taskSave = Task.Run(() => Logica.LogicaVehiculo.updateVehiculo(dataVehiculo));
                    taskSave.Wait();
                    if (taskSave.Result)
                    {
                        lblMessage.Text = "Vehiculo Actualizado y guardado correctamente";
                        newVehiculo();
                    }
                }


            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
        }

        protected void imgvGuardar_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            if (Request["cod"] != null)
            {

                updateVehiculo();
            }
            else
            {
                saveVehiculo();
            }

        }

        protected void lnkvGuardar_Click(object sender, EventArgs e)
        {
            if (Request["cod"] != null)
            {
                updateVehiculo();
            }
            else
            {
                saveVehiculo();
            }

        }
         private void regresar()
                {
                    Response.Redirect("wfmVehiculosList.aspx");

                }
        protected void imgbnuevo_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            newVehiculo();
        }

        protected void lnknuevo_Click(object sender, EventArgs e)
        {
            newVehiculo();
        }       

        protected void lnkvRegresar_Click(object sender, EventArgs e)
        {
            regresar();
        }

        protected void ImagevRegresar_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            regresar();
        }
    }
}