﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ventas_aspnet.Models;

namespace Ventas_aspnet.Public.Modelos
{
    public partial class wfmModeloList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                loadModelo();
            }
        }

        private void loadModelo()
        {
            try
            {
                var tasklistModelo = Task.Run(() => Logica.LogicaModelo.getModelo());
                tasklistModelo.Wait();

                var listModelo = tasklistModelo.Result;

                if (listModelo.Count > 0 && listModelo != null)
                {
                    UC_GridModelo1.GridView.DataSource = listModelo.Select(data => new
                    {
                        ID = data.modelo_id,
                        NOMBRE = data.mod_descripcion,
                        ESTADO = data.mod_status
                    }).ToList();
                    UC_GridModelo1.DataBind();

                }
            }
            catch (Exception ex)
            {
            }

        }
        protected void UC_GridModelo1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                string codigo = Convert.ToString(e.CommandArgument);
                if (e.CommandName.Equals("Modificar"))
                {
                    Response.Redirect("wfmModeloNuevo.aspx?cod=" + codigo);
                }
                else if (e.CommandName.Equals("Eliminar"))
                {
                    Modelo dataVehiculo = new Modelo();
                    var taskVehiculo = Task.Run(() => Logica.LogicaModelo.getModeloxId(int.Parse(codigo)));
                    taskVehiculo.Wait();
                    dataVehiculo = taskVehiculo.Result;
                    if (dataVehiculo != null)
                    {
                        var taskDelete = Task.Run(() => Logica.LogicaModelo.deleteModelo(dataVehiculo));
                        taskDelete.Wait();
                        if (taskDelete.Result)
                        {
                            loadModelo();
                        }
                    }


                }


            }
            catch (Exception)
            {

                throw;
            }
        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                string codigo = Convert.ToString(e.CommandArgument);
                if (e.CommandName.Equals("Modificar"))
                {
                    Response.Redirect("wfmModeloNuevo.aspx?cod=" + codigo);
                }
                else if (e.CommandName.Equals("Eliminar"))
                {
                    Modelo dataVehiculo = new Modelo();
                    var taskVehiculo = Task.Run(() => Logica.LogicaModelo.getModeloxId(int.Parse(codigo)));
                    taskVehiculo.Wait();
                    dataVehiculo = taskVehiculo.Result;
                    if (dataVehiculo != null)
                    {
                        var taskDelete = Task.Run(() => Logica.LogicaModelo.deleteModelo(dataVehiculo));
                        taskDelete.Wait();
                        if (taskDelete.Result)
                        {
                            loadModelo();
                        }
                    }


                }


            }
            catch (Exception)
            {

                throw;
            }
        }



        private void nuevo()
        {
            Response.Redirect("wfmModeloNuevo.aspx");
        }


        protected void lnkNuevo_Click(object sender, EventArgs e)
        {
            nuevo();
        }

        protected void imgNuevo_Click(object sender, ImageClickEventArgs e)
        {
            nuevo();
        }
    }
}