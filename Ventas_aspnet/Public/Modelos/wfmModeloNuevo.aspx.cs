﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ventas_aspnet.Models;

namespace Ventas_aspnet.Public.Modelos
{
    public partial class wfmModeloNuevo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request["cod"] != null)
                {
                    loadModelo(int.Parse(Request["cod"].ToString()));
                }
            }

        }
        private void loadModelo(int codigoProducto)
        {
            try
            {
                Modelo dataProducto = new Modelo();
                var tasProducto = Task.Run(() => Logica.LogicaModelo.getModeloxId(codigoProducto));
                tasProducto.Wait();
                dataProducto = tasProducto.Result;
                if (dataProducto != null)
                {
                    lblId.Text = dataProducto.modelo_id.ToString();
                    txtDescripcion.Text = dataProducto.mod_descripcion;                   
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;

            }
        }

        private void newModelo()
        {
            lblId.Text = "";
            txtDescripcion.Text = "";
        }

        private void save()
        {
            try
            {
                Modelo dataProducto = new Modelo();
                dataProducto.mod_descripcion = txtDescripcion.Text;
                var taskSave = Task.Run(() => Logica.LogicaModelo.saveModelo(dataProducto));
                taskSave.Wait();
                if (taskSave.Result)
                {
                    lblMessage.Text = "modelo guardado correctamente";
                    newModelo();
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
        }

        private void update()
        {
            try
            {
                Modelo dataProducto = new Modelo();
                var tasProducto = Task.Run(() => Logica.LogicaModelo.getModeloxId(int.Parse(lblId.Text)));
                tasProducto.Wait();
                dataProducto = tasProducto.Result;
                if (dataProducto != null)
                {
                    dataProducto.mod_descripcion = txtDescripcion.Text;
                    var taskSave = Task.Run(() => Logica.LogicaModelo.updateModelo(dataProducto));
                    taskSave.Wait();
                    if (taskSave.Result)
                    {
                        lblMessage.Text = "Modelo guardado correctamente";
                        newModelo();
                    }
                }


            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
        }

        protected void imgpnuevo_Click(object sender, ImageClickEventArgs e)
        {
            newModelo();
        }

        protected void lnknuevo_Click(object sender, EventArgs e)
        {
            newModelo();
        }

        protected void imgbGuardar_Click(object sender, ImageClickEventArgs e)
        {
            if (Request["cod"] != null)
            {

                update();
            }
            else
            {
                save();
            }

        }

        protected void lnkGuardar_Click(object sender, EventArgs e)
        {
            if (Request["cod"] != null)
            {

                update();
            }
            else
            {
                save();
            }

        }

        private void regresar()
        {
            Response.Redirect("wfmModeloList.aspx");

        }

        protected void ImageRegresar_Click(object sender, ImageClickEventArgs e)
        {
            regresar();
        }

        protected void lnkRegresar_Click(object sender, EventArgs e)
        {
            regresar();
        }
    }
}