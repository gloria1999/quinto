﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="wfmModeloList.aspx.cs" Inherits="Ventas_aspnet.Public.Modelos.wfmModeloList" %>
<%--k<%@ Register Src="~/UserControls/wfmGridView.ascx" TagName="UC_Grid" TagPrefix="UC1" %>--%>
<%@ Register Src="~/UserControls/ucVehiculoGridView.ascx" TagName="UC_GridModelo" TagPrefix="UC1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <div align="center" width="95%">
        <table>
            <tr>
                <td>
                    <h3>Lista Producto</h3>
                </td>
            </tr>
            <tr>
                <td>&nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:ImageButton ID="imgNuevo" ImageUrl="~/images/icon_nuevo.png" runat="server" Width="32px" Height="32px" OnClick="imgNuevo_Click" />
                    <asp:LinkButton ID="lnkNuevo" ImageUrl="~/images/icon_nuevo.png" runat="server" OnClick="lnkNuevo_Click"  >Nuevo</asp:LinkButton>
                </td>
            </tr>
            <tr>
                <td>&nbsp;
                </td>
            </tr>
                    <tr>
                <td align="center">                    
                    <UC1:UC_GridModelo id="UC_GridModelo1" runat="server" />                    
                </td>
            </tr>    
        </table>
    </div>
</asp:Content>