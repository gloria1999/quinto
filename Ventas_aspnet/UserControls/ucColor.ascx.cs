﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ventas_aspnet.Models;

namespace Ventas_aspnet.UserControls
{
    public partial class ucColor : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                UcCargarDatosColor();
            }
        }

        public DropDownList DropDownList
        {
            get
            {
                return DropDownList1;
            }
            set
            {
                DropDownList1 = value;
            }
        }
        private void UcCargarDatosColor()
        {
            var _taskColor = Task.Run(() => Logica.LogicaColor.getColor());
            _taskColor.Wait();
            var lisColor = _taskColor.Result;

            if (lisColor.Count > 0 && lisColor != null)
            {
                var data = lisColor.OrderBy(colores => colores.col_nombre).ToList();
                data.Insert(0, new Color { col_nombre = "seleccione Color", col_id = 0 });
                DropDownList1.DataSource = data;
                //mostrar al usuario
                DropDownList1.DataTextField = "col_nombre";
                //codigo
                DropDownList1.DataValueField = "col_id";
                DropDownList1.DataBind();
            }
        }
    }
}

