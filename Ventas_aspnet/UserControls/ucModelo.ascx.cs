﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ventas_aspnet.Models;

namespace Ventas_aspnet.UserControls
{
    public partial class ucModelo : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                UcCargarDatosModelo();
            }
        }
        public DropDownList DropDownList
        {
            get
            {
                return DropDownList1;
            }
            set
            {
                DropDownList1 = value;
            }
        }
        private void UcCargarDatosModelo()
        {
            var _taskModelo = Task.Run(() => Logica.LogicaModelo.getModelo());
            _taskModelo.Wait();
            var listaModelo = _taskModelo.Result;

            if (listaModelo.Count > 0 && listaModelo != null)
            {
                var data = listaModelo.OrderBy(modelos => modelos.mod_descripcion).ToList();
                data.Insert(0, new Modelo { mod_descripcion = "seleccione Modelo", modelo_id = 0 });
                DropDownList1.DataSource = data;
                //mostrar al usuario
                DropDownList1.DataTextField = "mod_descripcion";
                //codigo
                DropDownList1.DataValueField = "modelo_id";
                DropDownList1.DataBind();
            }
        }
    }
}

