﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ventas_aspnet.Models;

namespace Ventas_aspnet.UserControls
{
    public partial class ucModeloGridView : System.Web.UI.UserControl
    {
        public object Datasource { get; internal set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                loadData();
            }


        }
        public void loadData()
        {
            var taskListModelo = Task.Run(() => Logica.LogicaModelo.getModelo());
            taskListModelo.Wait();
            var listModelo = taskListModelo.Result;
            if (listModelo.Count > 0 && listModelo != null)
            {
                GridView1.DataSource = listModelo.Select(data => new
                {
                    ID = data.modelo_id,
                    DESCRIPCION = data.mod_descripcion,
                    ESTADO = data.mod_status
                }).ToList();
                GridView1.DataBind();
            }
        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                string codigo = Convert.ToString(e.CommandArgument);
                if (e.CommandName.Equals("Modificar"))
                {
                    Response.Redirect("wfmModeloNuevo.aspx?cod=" + codigo);
                }
                else if (e.CommandName.Equals("Eliminar"))
                {
                    Modelo dataModelo = new Modelo();
                    var tasModelo = Task.Run(() => Logica.LogicaModelo.getModeloxId(int.Parse(codigo)));
                    tasModelo.Wait();
                    dataModelo = tasModelo.Result;
                    if (dataModelo != null)
                    {
                        var taskDelete = Task.Run(() => Logica.LogicaModelo.deleteModelo(dataModelo));
                        taskDelete.Wait();
                        if (taskDelete.Result)
                        {
                            loadData();
                        }
                    }


                }


            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}