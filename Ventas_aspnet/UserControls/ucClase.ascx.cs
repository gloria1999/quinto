﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ventas_aspnet.Models;

namespace Ventas_aspnet.UserControls
{
    public partial class ucClase : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                UcCargarDatosClase();
            }
        }

        public DropDownList DropDownList
        {
            get
            {
                return DropDownList1;
            }
            set
            {
                DropDownList1 = value;
            }
        }
        private void UcCargarDatosClase()
        {
            var _taskClase = Task.Run(() => Logica.LogicaClase.getClases());
            _taskClase.Wait();
            var listaClase = _taskClase.Result;

            if (listaClase.Count > 0 && listaClase != null)
            {
                var data = listaClase.OrderBy(clases => clases.cla_descripcion).ToList();
                data.Insert(0, new Clase { cla_descripcion = "seleccione Clase", cla_id = 0 });
                DropDownList1.DataSource = data;
                //mostrar al usuario
                DropDownList1.DataTextField = "cla_descripcion";
                //codigo
                DropDownList1.DataValueField = "cla_id";
                DropDownList1.DataBind();
            }
        }
    }
}

