﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ventas_aspnet.Models;

namespace Ventas_aspnet.UserControls
{
    public partial class ucTipo : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                UcCargarDatosTipo();
            }
        }

        public DropDownList DropDownList
        {
            get
            {
                return DropDownList1;
            }
            set
            {
                DropDownList1 = value;
            }
        }
        private void UcCargarDatosTipo()
        {
            var _taskTipo = Task.Run(() => Logica.LogicaTipo.getTipo());
            _taskTipo.Wait();
            var listTipo = _taskTipo.Result;

            if (listTipo.Count > 0 && listTipo != null)
            {
                var data = listTipo.OrderBy(tipos => tipos.tip_descripcion).ToList();
                data.Insert(0, new Tipo { tip_descripcion = "seleccione tipo", tip_id = 0 });
                DropDownList1.DataSource = data;
                //mostrar al usuario
                DropDownList1.DataTextField = "tip_descripcion";
                //codigo
                DropDownList1.DataValueField = "tip_id";
                DropDownList1.DataBind();
            }
        }
    }
}