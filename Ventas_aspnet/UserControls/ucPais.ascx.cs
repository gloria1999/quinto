﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ventas_aspnet.Models;

namespace Ventas_aspnet.UserControls
{
    public partial class ucPais : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                UcCargarDatosPais();
            }
        }

        public DropDownList DropDownList
        {
            get
            {
                return DropDownList1;
            }
            set
            {
                DropDownList1 = value;
            }
        }
        private void UcCargarDatosPais()
        {
            var _taskPais = Task.Run(() => Logica.LogicaPais.getPais());
            _taskPais.Wait();
            var listPais = _taskPais.Result;

            if (listPais.Count > 0 && listPais != null)
            {
                var data = listPais.OrderBy(paises => paises.pai_nombre).ToList();
                data.Insert(0, new Pais { pai_nombre = "seleccione pais", pai_id = 0 });
                DropDownList1.DataSource = data;
                //mostrar al usuario
                DropDownList1.DataTextField = "pai_nombre";
                //codigo
                DropDownList1.DataValueField = "pai_id";
                DropDownList1.DataBind();
            }
        }
    }
}

