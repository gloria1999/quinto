﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ventas_aspnet.Models;

namespace Ventas_aspnet.UserControls
{
    public partial class wfmGridView : System.Web.UI.UserControl
    {
       // internal var Datasource;

        // internal var Datasource { get; set; }

        //internal object datasource;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                loadData();
            }
        }
        public void loadData()
        {
            var taskListProducts = Task.Run(() => Logica.LogicaProducto.getProducto());
            taskListProducts.Wait();
            var listaProducts = taskListProducts.Result;
            if (listaProducts.Count > 0 && listaProducts != null)
            {
                GridView1.DataSource = listaProducts.Select(data => new
                {
                    ID = data.pro_id,
                    CODIGO = data.pro_codigo,
                    NOMBRE = data.pro_nombre,
                    PRECIO_C = data.pro_preciocompra.ToString("0.00"),
                    PRECIO_V = data.pro_precioventa.ToString("0.00"),
                    STOCK_MIN = data.pro_stockminimo,
                    STOCK_MAX = data.pro_stockmaximo,
                    CATEGORIA = data.TBL_CATEGORIA.cat_nombre,
                    ESTADO = data.pro_status
                }).ToList();
                GridView1.DataBind();
            }
        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                string codigo = Convert.ToString(e.CommandArgument);
                if (e.CommandName.Equals("Modificar"))
                {
                    Response.Redirect("wfmProductoNuevo.aspx?cod=" + codigo);
                }
                else if (e.CommandName.Equals("Eliminar"))
                {
                    TBL_PRODUCTO dataProducto = new TBL_PRODUCTO();
                    var taskProducto = Task.Run(() => Logica.LogicaProducto.getProductoxId(int.Parse(codigo)));
                    taskProducto.Wait();
                    dataProducto = taskProducto.Result;
                    if (dataProducto != null)
                    {
                        var taskDelete = Task.Run(() => Logica.LogicaProducto.deleteProducto(dataProducto));
                        taskDelete.Wait();
                        if (taskDelete.Result)
                        {
                            loadData();
                            // loadProducto();
                        }
                    }


                }


            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}