﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ventas_aspnet.Models;

namespace Ventas_aspnet.UserControls
{
    public partial class ucCategoria : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                UcCargarDatos();
            }
        }

        public DropDownList DropDownList
        {
            get
            {
                return DropDownList1;
            }
            set
            {
                DropDownList1 = value;
            }
        }
        private void UcCargarDatos()
        {
            var _taskCategoria = Task.Run(() => Logica.LogicaCategoria.getAllCategoria());
            _taskCategoria.Wait();
            var listaCategoria = _taskCategoria.Result;

            if (listaCategoria.Count > 0 && listaCategoria != null)
            {
                var data = listaCategoria.OrderBy(categorias => categorias.cat_nombre).ToList();
                data.Insert(0, new TBL_CATEGORIA {cat_nombre = "seleccione categoria", cat_id = 0 });
                DropDownList1.DataSource = data;
                //mostrar al usuario
                DropDownList1.DataTextField = "cat_nombre";
                //codigo
                DropDownList1.DataValueField = "cat_id";
                DropDownList1.DataBind();
            }
        }
    }
}