﻿using Microsoft.EntityFrameworkCore;


namespace Matriculacion_Vehicular.Model
{
    public class Contexto:DbContext
    {
        public Contexto(DbContextOptions<Contexto> options) : base(options) { }
        public DbSet<Usuario> usuarios { get; set; }
        public DbSet<Rol> rols { get; set; }
    }
}
