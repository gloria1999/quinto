﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ActividadClase.Startup))]
namespace ActividadClase
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
