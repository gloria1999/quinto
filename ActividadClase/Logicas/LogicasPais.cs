﻿using ActividadClase.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace ActividadClase.Logicas
{
    public class LogicasPais
    {
        private static BDMATRICULAEntities db = new BDMATRICULAEntities();
        public static async Task<List<Pais>> getPais()
        {
            try
            {
                return await db.Pais.Where(data => data.pai_status == "A").ToListAsync();
            }
            catch (Exception ex)

            {
                throw new ArgumentException("Error al obtener pais");
            }
        }

        public static async Task<Pais> getPaisxId(int idPais)
        {
            try
            {
                return await db.Pais.FirstOrDefaultAsync(data => data.pai_status == "A"
                        && data.pai_id.Equals(idPais));
            }
            catch (Exception ex)

            {
                throw new ArgumentException("Error al obtener pais");
            }

        }
        public static async Task<bool> savePais(Pais dataPais)
        {
            try
            {

                bool result = false;

                dataPais.pai_status = "A";
                dataPais.pai_add = DateTime.Now;
                db.Pais.Add(dataPais);
                // dataProducto.Add(dataProducto);
                result = await db.SaveChangesAsync() > 0;
                //result = true;
                return result;
            }
            catch (Exception ex)

            {
                throw new ArgumentException("Error al guardar pais");
            }

        }
        public static async Task<bool> updatePais(Pais dataPais)
        {
            try
            {

                bool result = false;

                //dataProducto.pro_status = "A";
                dataPais.pai_add = DateTime.Now;
                // db.TBL_PRODUCTO.Add(dataProducto);
                result = await db.SaveChangesAsync() > 0;
                // result = true;
                return result;
            }
            catch (Exception ex)

            {
                throw new ArgumentException("Error al modificar pais");
            }

        }
        public static async Task<bool> deletePais(Pais dataPais)
        {
            try
            {

                bool result = false;

                dataPais.pai_status = "I";
                dataPais.pai_add = DateTime.Now;
                //db.TBL_PRODUCTO.Add(dataProducto);

                //result = await db.SaveChangesAsync() > 0;
                //eliminar datos de forma fisisca 
                //db.TBL_PRODUCTO.Remove(dataProducto);
                //commit hacia la base d edatos
                result = await db.SaveChangesAsync() > 0;
                //result = true;
                return result;
            }
            catch (Exception ex)

            {
                throw new ArgumentException("Error al guardar pais");
            }

        }
    }
}