﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="frmVehiculoNuevo.aspx.cs" Inherits="ActividadClase.Public.frmVehiculoNuevo" %>
<%@ Register Src="~/ucComboBox/ucComboBox.ascx" TagPrefix="UC2" TagName="UC_Clase" %>
<%@ Register Src="~/ucComboBox/ucComboBox.ascx" TagPrefix="UC4" TagName="UC_Modelo" %>
<%@ Register Src="~/ucComboBox/ucComboBox.ascx" TagPrefix="UC5" TagName="UC_Tipo" %>
<%@ Register Src="~/ucComboBox/ucComboBox.ascx" TagPrefix="UC3" TagName="UC_Color" %>
<%@ Register Src="~/ucComboBox/ucComboBox.ascx" TagPrefix="UC6" TagName="UC_Pais" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
 <table>
            <tr>
                <td colspan="2">
                    <h3><strong>Vehiculo Nuevo</strong> </h3>
                </td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table width="75" style="height:35px">
                        <tr>
                            <td>
                                <asp:imageButton ID="imgbnuevo" ImageUrl="~/images/icon_nuevo.png" runat="server"  Width="32px" Height="32px" CausesValidation="false" OnClick="imgbnuevo_Click" />
                                <asp:linkButton ID="lnknuevo" CausesValidation="false" runat="server" OnClick="lnknuevo_Click">nuevo</asp:linkButton>
                           
                                <td>
                                
                                <asp:ImageButton ID="imgvGuardar" ImageUrl="~/images/icon_guardar.png" runat="server" Width="32px" Height="32px" OnClick="imgvGuardar_Click" />
                                <asp:LinkButton ID="lnkvGuardar" runat="server" OnClick="lnkvGuardar_Click">Guardar</asp:LinkButton>
                            </td>
                            <td>
                                <asp:ImageButton ID="ImagevRegresar" ImageUrl="~/images/regresar.png" runat="server" Width="32px" Height="32px" CausesValidation="false" OnClick="ImagevRegresar_Click"/>
                                <asp:LinkButton ID="lnkvRegresar" CausesValidation="false" runat="server" OnClick="lnkvRegresar_Click">Regresar</asp:LinkButton>
                            </td>
                        </tr>
                    </table>
                </td>
                <td></td>
            </tr>
          <tr>
                <td>
                    <asp:Label ID="Label6" runat="server" Text="Id"></asp:Label></td>
                <td>
                    <asp:Label ID="lblId" runat="server" Text=""></asp:Label></td>
            </tr>
         
            
            <tr>
                <td>
                    <asp:Label ID="Label1" runat="server" Text="PLACA ANTERIOR"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtPlacaAnterior" runat="server" ControlToValidate="txtPlacaAnterior" ></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Placa Anterior Campo Obligatorio" ControlToValidate="txtPlacaAnterior" ForeColor="#CC0000" Text="*">*</asp:RequiredFieldValidator>
                </td>
            </tr>
         <tr>
                <td>
                    <asp:Label ID="Label8" runat="server" Text="PLACA ACTUAL"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtPlacaActual" runat="server" ControlToValidate="txtPlacaActual" ></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="Placa Actual Campo Obligatorio" ControlToValidate="txtPlacaActual" ForeColor="#CC0000" Text="*">*</asp:RequiredFieldValidator>
                </td>
            </tr>
          <tr>
                <td>
                    <asp:Label ID="Label10" runat="server" Text="CHASIS"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtChasis" runat="server" ControlToValidate="txtChasis" ></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="Chasis Campo Obligatorio" ControlToValidate="txtChasis" ForeColor="#CC0000" Text="*">*</asp:RequiredFieldValidator>
                </td>
            </tr>
          <tr>
                <td>
                    <asp:Label ID="Label12" runat="server" Text="MOTOR"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtMotor" runat="server" ControlToValidate="txtMotor" ></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="Motor Campo Obligatorio" ControlToValidate="txtMotor" ForeColor="#CC0000" Text="*">*</asp:RequiredFieldValidator>
                </td>
            </tr>
          <tr>
                <td>
                    <asp:Label ID="Label13" runat="server" Text="TONELAJE"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtTonelaje" runat="server" ControlToValidate="txtTonelaje" ></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="Tonelaje Campo Obligatorio" ControlToValidate="txtTonelaje" ForeColor="#CC0000" Text="*">*</asp:RequiredFieldValidator>
                </td>
            </tr>
          <tr>
                <td>
                    <asp:Label ID="Label14" runat="server" Text="FECHA COMPRA"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtFechaCompra" runat="server" ControlToValidate="txtFechaCompra" ></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="Fecha Compra Campo Obligatorio" ControlToValidate="txtFechaCompra" ForeColor="#CC0000" Text="*">*</asp:RequiredFieldValidator>
                </td>
            </tr>

           <tr>
                <td>
                    <asp:Label ID="Label7" runat="server" Text="PASAJEROS"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtPasajeros" runat="server" ControlToValidate="txtPasajeros" ></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Pasajeros Campo Obligatorio" ControlToValidate="txtPasajeros" ForeColor="#CC0000" Text="*">*</asp:RequiredFieldValidator>
                </td>
            </tr>
          <tr>
                <td>
                    <asp:Label ID="Label17" runat="server" Text="CARROCERIA"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtCarroceria" runat="server" ControlToValidate="txtCarroceria" ></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Carroceria Campo Obligatorio" ControlToValidate="txtCarroceria" ForeColor="#CC0000" Text="*">*</asp:RequiredFieldValidator>
                </td>
            </tr>

          <tr>
                <td>
                    <asp:Label ID="Label18" runat="server" Text="COMBUSTIBLE"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtCombustible" runat="server" ControlToValidate="txtCombustible" ></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Combustible Campo Obligatorio" ControlToValidate="txtCombustible" ForeColor="#CC0000" Text="*">*</asp:RequiredFieldValidator>
                </td>
            </tr>

          <tr>
                <td>
                    <asp:Label ID="Label19" runat="server" Text="OBSERVACION"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtObservacion" runat="server" ControlToValidate="txtObservacion" TextMode="MultiLine" Height="50px" Width="178px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Observacion Campo Obligatorio" ControlToValidate="txtObservacion" ForeColor="#CC0000" Text="*">*</asp:RequiredFieldValidator>
                </td>
            </tr>

          <tr>
                <td>
                    <asp:Label ID="Label15" runat="server" Text="FECHA EMISION"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtFechaCreacion" runat="server" ControlToValidate="txtFechaCreacion" ></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage="Fecha Creacion Campo Obligatorio" ControlToValidate="txtFechaCreacion" ForeColor="#CC0000" Text="*">*</asp:RequiredFieldValidator>
                </td>
            </tr>
          <tr>
                <td>
                    <asp:Label ID="Label20" runat="server" Text="FECHA CADUCIDAD"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtFechaCaducidad" runat="server" ControlToValidate="txtFechaCaducidad" ></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Fecha Caducidad Campo Obligatorio" ControlToValidate="txtFechaCaducidad" ForeColor="#CC0000" Text="*">*</asp:RequiredFieldValidator>
                </td>
            </tr>
          <tr>
                <td>
                    <asp:Label ID="Label16" runat="server" Text="AÑO"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtAnio" runat="server" ControlToValidate="txtAnio" ></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ErrorMessage="Año Campo Obligatorio" ControlToValidate="txtAnio" ForeColor="#CC0000" Text="*">*</asp:RequiredFieldValidator>
                </td>
            </tr>
               <tr>
                <td>
                    <asp:Label ID="Label21" runat="server" Text="CILINDRAJE"></asp:Label></td>
                <td>
                    <asp:TextBox ID="TxtCilindraje" runat="server" ControlToValidate="TxtCilindraje" ></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ErrorMessage="Cilindraje Campo Obligatorio" ControlToValidate="TxtCilindraje" ForeColor="#CC0000" Text="*">*</asp:RequiredFieldValidator>
                </td>
            </tr>


            <tr>
                <td>
                    <asp:Label ID="Label9" runat="server" Text="CLASE"></asp:Label></td>
                <td>
                    <UC2:UC_Clase ID="UC_Clase1" runat="server" />
                </td>
            </tr>

         <tr>
             <td>
                    <asp:Label ID="Label2" runat="server" Text="MODELO"></asp:Label></td>
                <td>
                    <UC4:UC_Modelo ID="UC_Modelo4" runat="server" />
                </td>
            </tr>

         <tr>
             <td>
                    <asp:Label ID="Label3" runat="server" Text="TIPO"></asp:Label></td>
                <td>
                    <UC5:UC_Tipo ID="UC_Tipo5" runat="server" />
                </td>
            </tr>

         <tr>
              <td>
                    <asp:Label ID="Label4" runat="server" Text="Color"></asp:Label></td>
                <td>
                    <UC3:UC_Color ID="UC_Color3" runat="server" />
                </td>
            </tr>
         <tr>
             <td>
                    <asp:Label ID="Label5" runat="server" Text="PAIS"></asp:Label></td>
                <td>
                    <UC6:UC_Pais ID="UC_Pais6" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label11" runat="server" Text=" MENSAJE"></asp:Label>
                </td>
                <td>
                    <asp:ValidationSummary  ID="ValidationSummary1" runat="server" ShowMessageBox="true" ForeColor="#CC0000" Height="41px" Width="189px" />
                    <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            </table>
</asp:Content>

