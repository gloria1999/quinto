﻿using ActividadClase.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ActividadClase.Public
{
    public partial class frmVehiculoNuevo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                string id = Request["cod"];
                if (id != null)
                {
                    ucCargarDatos();
                }


            }
            if (!IsPostBack)
            {
                UcCargarDatosClase();
                UcCargarDatosColor();
                UcCargarDatosModelo();
                UcCargarDatosTipo();
                UcCargarDatosPais();

            }
        }
        private void UcCargarDatosClase()
        {
            var _taskClase = Task.Run(() => Logicas.LogicasClase.getClases());
            _taskClase.Wait();
            var listaClase = _taskClase.Result;

            if (listaClase.Count > 0 && listaClase != null)
            {
                var data = listaClase.OrderBy(clases => clases.cla_descripcion).ToList();
                data.Insert(0, new Clase { cla_descripcion = "seleccione Clase", cla_id = 0 });
                UC_Clase1.DropDownList.DataSource = data;
                //mostrar al usuario
                UC_Clase1.DropDownList.DataTextField = "cla_descripcion";
                //codigo
                UC_Clase1.DropDownList.DataValueField = "cla_id";
                UC_Clase1.DropDownList.DataBind();
            }
        }
        private void UcCargarDatosColor()
        {
            var _taskColor = Task.Run(() => Logicas.LogicasColor.getColor());
            _taskColor.Wait();
            var lisColor = _taskColor.Result;

            if (lisColor.Count > 0 && lisColor != null)
            {
                var data = lisColor.OrderBy(colores => colores.col_nombre).ToList();
                data.Insert(0, new Color { col_nombre = "seleccione Color", col_id = 0 });
                UC_Color3.DropDownList.DataSource = data;
                //mostrar al usuario
                UC_Color3.DropDownList.DataTextField = "col_nombre";
                //codigo
                UC_Color3.DropDownList.DataValueField = "col_id";
                UC_Color3.DropDownList.DataBind();
            }
        }
        private void UcCargarDatosModelo()
        {
            var _taskModelo = Task.Run(() => Logicas.LogicasModelo.getModelo());
            _taskModelo.Wait();
            var listaModelo = _taskModelo.Result;

            if (listaModelo.Count > 0 && listaModelo != null)
            {
                var data = listaModelo.OrderBy(modelos => modelos.mod_descripcion).ToList();
                data.Insert(0, new Modelo { mod_descripcion = "seleccione Modelo", modelo_id = 0 });
                UC_Modelo4.DropDownList.DataSource = data;
                //mostrar al usuario
                UC_Modelo4.DropDownList.DataTextField = "mod_descripcion";
                //codigo
                UC_Modelo4.DropDownList.DataValueField = "modelo_id";
                UC_Modelo4.DropDownList.DataBind();
            }
        }
        private void UcCargarDatosPais()
        {
            var _taskPais = Task.Run(() => Logicas.LogicasPais.getPais());
            _taskPais.Wait();
            var listPais = _taskPais.Result;

            if (listPais.Count > 0 && listPais != null)
            {
                var data = listPais.OrderBy(paises => paises.pai_nombre).ToList();
                data.Insert(0, new Pais { pai_nombre = "seleccione pais", pai_id = 0 });
                UC_Pais6.DropDownList.DataSource = data;
                //mostrar al usuario
                UC_Pais6.DropDownList.DataTextField = "pai_nombre";
                //codigo
                UC_Pais6.DropDownList.DataValueField = "pai_id";
                UC_Pais6.DropDownList.DataBind();
            }
        }
        private void UcCargarDatosTipo()
        {
            var _taskTipo = Task.Run(() => Logicas.LogicasTipo.getTipo());
            _taskTipo.Wait();
            var listTipo = _taskTipo.Result;

            if (listTipo.Count > 0 && listTipo != null)
            {
                var data = listTipo.OrderBy(tipos => tipos.tip_descripcion).ToList();
                data.Insert(0, new Tipo { tip_descripcion = "seleccione tipo", tip_id = 0 });
                UC_Tipo5.DropDownList.DataSource = data;
                //mostrar al usuario
                UC_Tipo5.DropDownList.DataTextField = "tip_descripcion";
                //codigo
                UC_Tipo5.DropDownList.DataValueField = "tip_id";
                UC_Tipo5.DropDownList.DataBind();
            }
        }
        private string Decrypt(string cipherText)
        {
            string EncryptionKey = "MAKV2SPBNI99212";
            cipherText = cipherText.Replace(" ", "+");
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }
        private void ucCargarDatos()
        {

            string decry = Decrypt(HttpUtility.UrlDecode(Request.QueryString["cod"]));
            var taskUsuarioxId = Task.Run(() => Logicas.LogicasVehiculo.getVehiculoxId(Convert.ToInt32(decry)));
            taskUsuarioxId.Wait();
            var dataVehiculo = taskUsuarioxId.Result;
            lblId.Text = dataVehiculo.veh_id.ToString();
            txtPlacaAnterior.Text = dataVehiculo.veh_placaanterior;
            txtPlacaActual.Text = dataVehiculo.veh_placaactual;
            txtChasis.Text = dataVehiculo.veh_chasis;
            txtMotor.Text = dataVehiculo.veh_motor;
            txtTonelaje.Text = dataVehiculo.veh_tonelaje.ToString("0.00");
            txtAnio.Text = dataVehiculo.vehiculo_anio.ToString();
            TxtCilindraje.Text = dataVehiculo.veh_cilindraje.ToString();
            UC_Clase1.DropDownList.SelectedValue = dataVehiculo.cla_id.ToString();
            UC_Modelo4.DropDownList.SelectedValue = dataVehiculo.modelo_id.ToString();
            UC_Tipo5.DropDownList.SelectedValue = dataVehiculo.tip_id.ToString();
            UC_Color3.DropDownList.SelectedValue = dataVehiculo.col_id.ToString();
            UC_Pais6.DropDownList.SelectedValue = dataVehiculo.pai_id.ToString();
            txtPlacaAnterior.Enabled = false;
        }
        private void newVehiculo()
        {
            lblId.Text = "";
            txtPlacaAnterior.Text = "";
            txtPlacaActual.Text = "";
            txtChasis.Text = "";
            txtMotor.Text = "";
            txtTonelaje.Text = "";
            txtAnio.Text = "";
            UC_Clase1.DropDownList.SelectedIndex = 0;
            UC_Modelo4.DropDownList.SelectedIndex = 0;
            UC_Tipo5.DropDownList.SelectedIndex = 0;
            UC_Color3.DropDownList.SelectedIndex = 0;
            UC_Pais6.DropDownList.SelectedIndex = 0;
            txtPlacaAnterior.Enabled = true;

        }
        private void saveVehiculo()
        {
            try
            {
                Vehiculo dataVehiculo = new Vehiculo();
                dataVehiculo.cla_id = Convert.ToInt16(UC_Clase1.DropDownList.SelectedValue);
                dataVehiculo.modelo_id = Convert.ToInt16(UC_Modelo4.DropDownList.SelectedValue);
                dataVehiculo.tip_id = Convert.ToInt16(UC_Tipo5.DropDownList.SelectedValue);
                dataVehiculo.col_id = Convert.ToInt16(UC_Color3.DropDownList.SelectedValue);
                dataVehiculo.pai_id = Convert.ToInt16(UC_Pais6.DropDownList.SelectedValue);
                dataVehiculo.veh_placaanterior = txtPlacaAnterior.Text;
                // validar si existe el codigo
                var codexiste = Task.Run(() => Logicas.LogicasVehiculo.getVehiculoByCode(dataVehiculo.veh_placaanterior)).Result;
                if (codexiste != null)
                {
                    lblMessage.Text = "Codigo ya Asignado";
                    return;
                }
                dataVehiculo.veh_placaactual = txtPlacaActual.Text;
                dataVehiculo.veh_chasis = txtChasis.Text;
                dataVehiculo.veh_motor = txtMotor.Text;
                dataVehiculo.veh_tonelaje = decimal.Parse(txtTonelaje.Text);
                dataVehiculo.vehiculo_anio = Convert.ToInt16(txtAnio.Text);
                var taskSave = Task.Run(() => Logicas.LogicasVehiculo.saveVehiculo(dataVehiculo));
                taskSave.Wait();
                if (taskSave.Result)
                {
                    lblMessage.Text = "Vehiculo guardado correctamente";
                    newVehiculo();
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
        }

        private void updateVehiculo()
        {
            try
            {
                Vehiculo dataVehiculo = new Vehiculo();
                var tasVehiculo = Task.Run(() => Logicas.LogicasVehiculo.getVehiculoxId(int.Parse(lblId.Text)));
                tasVehiculo.Wait();
                dataVehiculo = tasVehiculo.Result;
                if (dataVehiculo != null)
                {
                    dataVehiculo.cla_id = Convert.ToInt16(UC_Clase1.DropDownList.SelectedValue);
                    dataVehiculo.modelo_id = Convert.ToInt16(UC_Modelo4.DropDownList.SelectedValue);
                    dataVehiculo.tip_id = Convert.ToInt16(UC_Tipo5.DropDownList.SelectedValue);
                    dataVehiculo.col_id = Convert.ToInt16(UC_Color3.DropDownList.SelectedValue);
                    dataVehiculo.pai_id = Convert.ToInt16(UC_Pais6.DropDownList.SelectedValue);
                    dataVehiculo.veh_placaanterior = txtPlacaAnterior.Text;
                    dataVehiculo.veh_placaactual = txtPlacaActual.Text;
                    dataVehiculo.veh_chasis = txtChasis.Text;
                    dataVehiculo.veh_motor = txtMotor.Text;
                    dataVehiculo.veh_tonelaje = decimal.Parse(txtTonelaje.Text);
                    dataVehiculo.vehiculo_anio = Convert.ToInt16(txtAnio.Text);
                    var taskSave = Task.Run(() => Logicas.LogicasVehiculo.updateVehiculo(dataVehiculo));
                    taskSave.Wait();
                    if (taskSave.Result)
                    {
                        lblMessage.Text = "Vehiculo Actualizado y guardado correctamente";
                        newVehiculo();
                    }
                }


            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
        }




        protected void lnkvGuardar_Click(object sender, EventArgs e)
        {
            if (Request["cod"] != null)
            {

                updateVehiculo();
            }
            else
            {
                saveVehiculo();
            }
        }

        protected void imgvGuardar_Click(object sender, ImageClickEventArgs e)
        {
            if (Request["cod"] != null)
            {

                updateVehiculo();
            }
            else
            {
                saveVehiculo();
            }
        }

        protected void lnkvRegresar_Click(object sender, EventArgs e)
        {
            Response.Redirect("frmVehiculoLista.aspx");
        }

        protected void ImagevRegresar_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("frmVehiculoLista.aspx");
        }

        protected void imgbnuevo_Click(object sender, ImageClickEventArgs e)
        {
            newVehiculo();
        }

        protected void lnknuevo_Click(object sender, EventArgs e)
        {
            newVehiculo();
        }
    }
}