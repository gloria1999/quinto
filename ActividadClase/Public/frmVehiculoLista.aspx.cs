﻿using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ActividadClase.Model;

namespace ActividadClase.Public
{
    public partial class frmVehiculoLista : System.Web.UI.Page
    {
        BDMATRICULAEntities db = new BDMATRICULAEntities();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                loadVehiculo();
            }
        }
        private void loadVehiculo()
        {
            try
            {
                var taskListVehiculo = Task.Run(() => Logicas.LogicasVehiculo.getVehiculo());
                taskListVehiculo.Wait();
                var listVehiculo = taskListVehiculo.Result;
                if (listVehiculo.Count > 0 && listVehiculo != null)
                {
                    gdvDatosVehiculo.DataSource = listVehiculo.Select(data => new
                    {
                        ID = data.veh_id,
                        PLACAANTERIOR = data.veh_placaanterior,
                        PLACAACTUAL = data.veh_placaactual,
                        CHASIS = data.veh_chasis,
                        MOTOR = data.veh_motor,
                        TONELAJE = data.veh_tonelaje.ToString("0.00"),
                        FECHACOMPRA = data.veh_fechacompra,
                        FECHA_CREACION = data.veh_add,
                        ANIO = data.vehiculo_anio,
                        CILINDRAJE = data.veh_carroceria,
                        CLASE = data.cla_id,
                        MODELO = data.modelo_id,
                        TIPO = data.tip_id,
                        COLOR = data.col_id,
                        PAIS = data.pai_id,
                        ESTADO = data.veh_status
                    }).ToList();
                    gdvDatosVehiculo.DataBind();
                }
            }
            catch (Exception ex)
            {
            }

        }

        private string Encrypt(string clearText)
        {
            string EncryptionKey = "MAKV2SPBNI99212";
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;
        }

        protected void lnkcNuevo_Click(object sender, EventArgs e)
        {
            Response.Redirect("frmVehiculoNuevo.aspx");
        }

        protected void imgcNuevo_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("frmVehiculoNuevo.aspx");
        }



        protected void gdvDatosVehiculo_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                string image = HttpUtility.UrlEncode(Encrypt(e.CommandArgument.ToString()));
                string codigoVehiculo = Convert.ToString(e.CommandArgument);
                if (e.CommandName.Equals("Modificar"))
                {
                    Response.Redirect("frmVehiculoNuevo.aspx?cod=" + image);
                }
                else if (e.CommandName.Equals("Eliminar"))
                {
                    Vehiculo dataVehiculo = new Vehiculo();
                    var taskVehiculo = Task.Run(() => Logicas.LogicasVehiculo.getVehiculoxId(int.Parse(codigoVehiculo)));
                    taskVehiculo.Wait();
                    dataVehiculo = taskVehiculo.Result;
                    if (dataVehiculo != null)
                    {
                        var taskDelete = Task.Run(() => Logicas.LogicasVehiculo.deleteVehiculo(dataVehiculo));
                        taskDelete.Wait();
                        if (taskDelete.Result)
                        {
                            loadVehiculo();
                        }
                    }


                }


            }
            catch (Exception)
            {

                throw;
            }

        }
    }
}